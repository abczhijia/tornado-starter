# coding:utf8
from .base import BaseHandler
from tornado.gen import coroutine, Return


class IndexHandler(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        user = yield self.user_srv.get_user_by_id(1)
        self.render('index.html', user=user)
