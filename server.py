# coding:utf8
from __future__ import print_function
import tornado.ioloop
import tornado.web
from config.app_config import configs
from handler import index
from tornado.options import options, define
from utils.log import log
import os

define("port", default=8888, help="run on the given port", type=int)
define('application_name', default='pinpin', type=str)
define('debug', default=False, type=bool)
define('mc_prefix', default='yeah', help='set the mc name', type=str)
define('dev', default=True, type=bool)
tornado.options.parse_command_line()


class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            gzip=True,
            debug=True,
            cookie_secret="123",
            # xsrf_cookies=True,
            template_path=os.path.join(os.path.dirname(__file__), 'front/template'),
            static_path=os.path.join(os.path.dirname(__file__), 'front/static'),
        )

        handlers = [(r"/", index.IndexHandler)]

        if options.dev:
            print('dev mode')
            # from handler.base import LoginHandler, LogoutHandler
            # handlers.append((r"/login", LoginHandler))
        #     handlers.append((r"/logout", LogoutHandler))

        super(Application, self).__init__(handlers=handlers, **settings)

    def log_request(self, handler):
        log.info('log request method')
        if 'log_function' in self.settings:
            self.settings['log_function'](handler)
            return
        if handler.get_status() < 400:
            log_method = log.info
        elif handler.get_status() < 500:
            log_method = log.warn
        else:
            log_method = log.error
        request_time = 1000.0 * handler.request.request_time()
        log_method(
            'statusCode:%d' % handler.get_status(),
            handler._request_summary(),
            'errorCode:%s' % handler.get_app_err_code(),
            'use_time:%.2fms' % float(request_time),
            'queueSize:', len(tornado.ioloop.IOLoop.instance()._events) + 1,
            'header:', handler.request.headers
        )
        # if handler._request_summary() is not None and hasattr(handler, 'get_app_err_code'):
        #     log_method(
        #         'statusCode:%d' % handler.get_status(),
        #         handler._request_summary(),
        #         'errorCode:%s' % handler.get_app_err_code(),
        #         'use_time:%.2fms' % float(request_time),
        #         'queueSize:', len(tornado.ioloop.IOLoop.instance()._events) + 1,
        #         'header:', handler.request.headers
        #     )


def main():
    http_server = tornado.httpserver.HTTPServer(Application(), xheaders=True)
    http_server.listen(options.port)
    io_loop = tornado.ioloop.IOLoop.instance()
    io_loop.start()

if __name__ == '__main__':
    main()

# def make_app():
#     return tornado.web.Application([
#         (r"/", index.IndexHandler),
#     ], **configs)
#
#
# if __name__ == "__main__":
#     port = 8888
#     app = make_app()
#     app.listen(port)
#     print("server is listening on http://localhost:%s" % port)
#     tornado.ioloop.IOLoop.current().start()
