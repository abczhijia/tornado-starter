# -*- coding: utf-8 -*-

import time
import errno
import socket
import random
import struct
import logging
from datetime import timedelta
from tornado import gen
from tornado.ioloop import IOLoop
from tornado.iostream import IOStream, StreamClosedError

from random import choice
from collections import deque
from __proto import Request, Response
from __jce import JceInputStream, JceOutputStream


def retry(func):
    def _(ins, *args, **kwargs):
        count = 2
        while count > 0:
            try:
                if ins.closed:
                    ins.reconnect()
                return func(ins, *args, **kwargs)
            except Exception as e:
                logging.exception(u"Error %s, retrying ...." % e)
                ins.close()
                count -= 1
    return _


def check_if_int(*args):
    '''int, str_int, unicode_int 检查及转换helper函数'''
    int_args = []
    for i in args:
        if isinstance(i, (int, long)):
            int_args.append(i)
        elif isinstance(i, (str, unicode)) and i.isdigit():
            int_args.append(int(i))
        else:
            return True, args
    return False, int_args


def safe_read(s):
    buf = s.recv(4, socket.MSG_WAITALL)
    pack_len = struct.unpack("!I", buf)[0] if buf else 0
    read_len, ret = 0, ""
    while pack_len > read_len:
        try:
            buf = s.recv(pack_len - read_len, socket.MSG_WAITALL)
            ret += buf
            read_len += len(buf)
        except socket.error, e:
            if e.args[0] in (errno.EWOULDBLOCK, errno.EAGAIN):
                continue
            else:
                raise
    return ret


def safe_write(s, buf):
    s.sendall(struct.pack("!I", len(buf)) + buf)


class Client(object):

    def __init__(self, hosts=(), version=0, timeout=3):
        self.hosts = hosts
        self.closed = True
        self.timeout = timeout
        self.version = version
        self.reconnect()

    def reconnect(self):
        self._sockets = [self.init_socket(host, self.timeout) for host in self.hosts]
        self.closed = False

    def init_socket(self, host, timeout=3):
        ip, port = host.split(':')
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        s.settimeout(timeout)
        s.connect((ip, int(port)))
        return s

    def close(self):
        [s.close() for s in self._sockets]
        self.closed = True

    def do_proc(self, function, kwargs):
        req = Request(ver=self.version, function=function, kwargs=kwargs)
        oos = JceOutputStream()
        req.writeTo(oos, req)
        s = random.choice(self._sockets)
        safe_write(s, oos.getBuffer())
        buf = safe_read(s)
        ios = JceInputStream(buf)
        rsp = Response.readFrom(ios)
        if rsp.code != 0:
            logging.error('error: function called error in %s, args: %s' % (function, kwargs))
            return {}
        return rsp.result

    @retry
    def _process_user_request(self, cmd, kwargs):
        if 'uid' not in kwargs:
            logging.error('error input in circle_rpc_cli.%s args: %s' % (cmd, kwargs))
            return {}
        err, (kwargs['uid'], ) = check_if_int(kwargs['uid'])
        if err:
            logging.error('error input in circle_rpc_cli.%s' % cmd)
            return {}
        return self.do_proc(cmd, kwargs)

    @retry
    def _process_users_request(self, cmd, kwargs):
        if 'uids' not in kwargs:
            logging.error('error input in circle_rpc_cli.%s args: %s' % (cmd, kwargs))
            return {}
        err, uids = check_if_int(*kwargs['uids'])
        if err:
            logging.error('error input in circle_rpc_cli.%s' % cmd)
            return {}
        if isinstance(uids, tuple):
            uids = list(uids)
        kwargs['uids'] = uids
        return self.do_proc(cmd, kwargs)


class TimeoutError(Exception):
    pass


class ConnectionTimeoutError(TimeoutError):
    pass


class ReadTimeoutError(TimeoutError):
    pass


class WriteTimeoutError(TimeoutError):
    pass


class PoolClient(object):

    def __init__(self, pool, server_list, socket_timeout=5):
        assert server_list
        self.io_loop = IOLoop.current()
        self._pool = pool
        self._network = None
        self._server_list = server_list
        self._socket_timeout = socket_timeout
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.stream = IOStream(sock)

    @gen.coroutine
    def connect(self):
        _host, _port = self.round_robin()
        future = self.stream.connect((_host, int(_port)))
        yield self.handle_timeout(future)
        local_ip, local_port = self.stream.socket.getsockname()
        self._network = '%s:%s' % (local_ip, local_port)

    def close(self):
        self.stream.close()

    @property
    def closed(self):
        return self.stream.closed()

    @property
    def network(self):
        return self._network

    def round_robin(self):
        addr = choice(self._server_list)
        pair = addr.split(':')
        return pair[0], int(pair[1])

    @gen.coroutine
    def write(self, data):
        future = self.stream.write(data)
        yield self.handle_timeout(future)

    @gen.coroutine
    def read_bytes(self, length):
        future = self.stream.read_bytes(length)
        response = yield self.handle_timeout(future)
        raise gen.Return(response)

    @gen.coroutine
    def handle_timeout(self, future):
        response = yield gen.with_timeout(
            timedelta(seconds=self._socket_timeout), future, quiet_exceptions=StreamClosedError)
        raise gen.Return(response)

    @gen.coroutine
    def do_proc(self, function, kwargs):
        req = Request(0x03, function, kwargs)
        oos = JceOutputStream()
        Request.writeTo(oos, req)
        buffer = oos.getBuffer()
        yield self.write(struct.pack("!I", len(buffer)) + buffer)
        buf = yield self.read_resp()

        ios = JceInputStream(buf)
        rsp = Response.readFrom(ios)
        if rsp.code != 0:
            logging.error('error: async function called error in %s, args: %s' % (function, kwargs))
            raise gen.Return({})
        raise gen.Return(rsp.result)

    @gen.coroutine
    def read_resp(self):
        buf = yield self.read_bytes(4)
        pack_len = struct.unpack("!I", buf)[0]
        ret = yield self.read_bytes(pack_len)
        raise gen.Return(ret)


class PoolExhaustedError(Exception):
    pass


class PoolClosedError(Exception):
    pass


class Pool(object):

    def __init__(self, server_list, socket_timeout=3, max_idle=5, max_active=10, idle_timeout=600):
        self.server_list = server_list
        self.socket_timeout = socket_timeout
        self.max_idle = max_idle
        self.max_active = max_active
        self.idle_timeout = idle_timeout
        self.active = 0
        self.idle_queue = deque()
        self.closed = False

    def check_idle_client_timeout(self):
        while self.idle_queue:
            conn = self.idle_queue[0]
            if not conn or conn.idle_at + self.idle_timeout > time.time():
                break
            conn = self.idle_queue.popleft()
            if not conn.closed:
                conn.close()
            logging.info('%s prune idle client, current active clients count: %s idle clients count: %s'
                         % (self.server_list, self.active, len(self.idle_queue)))

    def put(self, client):
        self.active -= 1
        if self.closed:
            logging.info('%s put back but pool has closed, current active clients count: %s idle clients count: %s'
                         % (self.server_list, self.active, len(self.idle_queue)))
            client.close()
            return
        if client.closed:
            logging.info('%s put back but client has closed, current active clients count: %s idle clients count: %s'
                         % (self.server_list, self.active, len(self.idle_queue)))
            return
        self.check_idle_client_timeout()
        current_idle_num = len(self.idle_queue)
        if current_idle_num > self.max_idle:
            client.close()
            logging.info('%s current idle clients exceed: %s' % (self.server_list, current_idle_num))
            return
        client.idle_at = time.time()
        self.idle_queue.append(client)

    @gen.coroutine
    def get_client(self):
        if self.closed:
            raise PoolClosedError("connection pool closed.")

        if self.active > self.max_active:
            raise PoolExhaustedError("connection pool exhausted. active: %d" % self.active)

        self.check_idle_client_timeout()

        client = None
        if self.idle_queue:
            client = self.idle_queue.pop()
        if not client:
            client = PoolClient(self, self.server_list, self.socket_timeout)
            yield client.connect()
            logging.info('%s create new rpc client, current active clients count: %s idle clients count: %s'
                         % (self.server_list, self.active + 1, len(self.idle_queue)))
        self.active += 1
        raise gen.Return(client)

    def close(self):
        self.closed = True
        while len(self.idle_queue) > 0:
            c = self.idle_queue.popleft()
            c.close()


class AsyncClient(object):

    def __init__(self, server_list, socket_timeout=2):
        assert server_list
        self.pool = Pool(server_list, socket_timeout)

    def close(self):
        self.pool.close()

    @gen.coroutine
    def do_proc(self, cmd, kwargs, retry_time=1):
        # 异常情况，默认重试一次
        while retry_time >= 0:
            c = yield self.pool.get_client()
            try:
                rsp = yield c.do_proc(cmd, kwargs)
            except Exception as e:
                logging.exception('AsyncClient %s do_proc cmd: %s kwargs: %s error: %s'
                                  % (c.network, cmd, kwargs, str(e)))
                c.close()
                if retry_time == 0:
                    raise
                retry_time -= 1
            else:
                raise gen.Return(rsp)
            finally:
                self.pool.put(c)

    @gen.coroutine
    def _process_user_request(self, cmd, kwargs):
        if 'uid' not in kwargs:
            logging.error('error input in circle_rpc_cli.%s args: %s' % (cmd, kwargs))
            raise gen.Return({})
        err, (kwargs['uid'], ) = check_if_int(kwargs['uid'])
        if err:
            logging.error('error input in circle_rpc_cli.%s' % cmd)
            raise gen.Return({})
        rsp = yield self.do_proc(cmd, kwargs)
        raise gen.Return(rsp)

    @gen.coroutine
    def _process_users_request(self, cmd, kwargs):
        if 'uids' not in kwargs:
            logging.error('error input in circle_rpc_cli.%s args: %s' % (cmd, kwargs))
            raise gen.Return({})
        err, uids = check_if_int(*kwargs['uids'])
        if err:
            logging.error('error input in circle_rpc_cli.%s' % cmd)
            raise gen.Return({})
        if isinstance(uids, tuple):
            uids = list(uids)
        kwargs['uids'] = uids
        rsp = yield self.do_proc(cmd, kwargs)
        raise gen.Return(rsp)
