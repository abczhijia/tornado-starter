# -*- coding: utf-8 -*-


class Boolean(object):
    __taf_index__ = 999
    __taf_class__ = 'bool'
    __slots__ = ('__taf_index__', '__taf_class__')


class Int8(object):
    __taf_index__ = 0
    __taf_class__ = 'char'
    __slots__ = ('__taf_index__', '__taf_class__')


class Uint8(object):
    __taf_index__ = 1
    __taf_class__ = 'short'
    __slots__ = ('__taf_index__', '__taf_class__')


class Int16(object):
    __taf_index__ = 1
    __taf_class__ = 'short'
    __slots__ = ('__taf_index__', '__taf_class__')


class Uint16(object):
    __taf_index__ = 2
    __taf_class__ = 'int32'
    __slots__ = ('__taf_index__', '__taf_class__')


class Int32(object):
    __taf_index__ = 2
    __taf_class__ = 'int32'
    __slots__ = ('__taf_index__', '__taf_class__')


class Uint32(object):
    __taf_index__ = 3
    __taf_class__ = 'int64'
    __slots__ = ('__taf_index__', '__taf_class__')


class Int64(object):
    __taf_index__ = 3
    __taf_class__ = 'int64'
    __slots__ = ('__taf_index__', '__taf_class__')


class Float(object):
    __taf_index__ = 4
    __taf_class__ = 'float'
    __slots__ = ('__taf_index__', '__taf_class__')


class Double(object):
    __taf_index__ = 5
    __taf_class__ = 'double'
    __slots__ = ('__taf_index__', '__taf_class__')


class Bytes(object):
    __taf_index__ = 13
    __taf_class__ = 'list<char>'
    __slots__ = ('__taf_index__', '__taf_class__')


class String(object):
    __taf_index__ = 67
    __taf_class__ = 'string'
    __slots__ = ('__taf_index__', '__taf_class__')


class Struct(object):
    __taf_index__ = 1011
    __slots__ = ('__taf_index__', )
