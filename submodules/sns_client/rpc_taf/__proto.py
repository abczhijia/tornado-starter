# -*- coding: utf-8 -*-

import json

from __types import Int32, String, Struct


class Request(Struct):
    __slots__ = ('ver', 'function', 'kwargs')

    def __init__(self, ver=0, function='', kwargs=''):
        self.ver = ver
        self.function = function
        self.kwargs = kwargs

    @staticmethod
    def writeTo(oos, req):
        oos.write(Int32, 0, req.ver)
        oos.write(String, 1, req.function)
        oos.write(String, 2, json.dumps(req.kwargs))

    @staticmethod
    def readFrom(ios):
        req = Request()
        req.ver = ios.read(Int32, 0, True, req.ver)
        req.function = ios.read(String, 1, True, req.function)
        req.kwargs = json.loads(ios.read(String, 2, True, req.kwargs))
        return req


class Response(Struct):
    __slots__ = ('code', 'result')

    def __init__(self, code=0, result=None):
        self.code = code
        self.result = result

    @staticmethod
    def writeTo(oos, rsp):
        oos.write(Int32, 0, rsp.code)
        oos.write(String, 1, json.dumps(rsp.result))

    @staticmethod
    def readFrom(ios):
        rsp = Response()
        rsp.code = ios.read(Int32, 0, True, rsp.code)
        rsp.result = json.loads(ios.read(String, 1, True, rsp.result))
        return rsp
