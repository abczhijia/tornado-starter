#!/usr/bin/env python
# -*- coding: utf-8

"""
屏蔽列表接口
"""

import socket
import logging
import ast
from random import choice

DEFAULT_EN_HOST_LIST = ["10.221.236.76:6000"]

class EnvoyClient(object):
    def __init__(self):
        self._server_list = DEFAULT_EN_HOST_LIST
        self._socket = None
        self.reconnect()

    def reconnect(self):
        if self._socket:
            self.close()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host, port = self.round_robin()
        s.settimeout(3)
        s.connect((host, port))
        self._socket = s

    def remove_from_unsublist(self, my_user_id, user_id):
        req = ('{"my_user_id": %s, "user_id": %s}\n' % (my_user_id, user_id)).encode('utf-8')
        try:
            return self._fetch(req)
        except Exception, e:
            logging.warn("Reconnect.")
            self.reconnect()
            return self._fetch(req)

    def _fetch(self, req):
        self._socket.sendall(req)
        data = self._socket.recv(4096)
        result = ast.literal_eval(data)
        return result

    def close(self):
        self._socket.close()
        self._socket = None

    def round_robin(self):
        addr = choice(self._server_list)
        pair = addr.split(':')
        return pair[0], int(pair[1])

if __name__ == '__main__':
    c = EnvoyClient()
    s = c.remove_from_unsublist(12027193,1)
    print "result:", s
    c.close()
    
