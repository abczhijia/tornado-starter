# -*- coding: utf-8 -*-
import redis

PREFIX = "SUB:"
actions = ("up", "publish", "inspect", "comment", "self_pub")

REDIS_CONFIG = [{'host': '10.225.44.164', 'port': 16379}, {'host': '10.207.169.66', 'port': 16379}]


def _get_data(action, data):
    if action == "comment":
        spits = data.split(',')
        return {"user_id": long(spits[0]), "level": int(spits[1]), "comment": "".join(spits[2:])} if len(spits) > 2 else ({"user_id": long(spits[0])} if len(spits) == 1 else None)
    if action == "up" or action == 'publish':
        return {"user_id": long(data)}
    return {}


def _check_type(record, fmt):
    if fmt:
        return record[-1] in fmt
    else:
        return True


def _get_art_id(record):
    if record[-1].isdigit():
        return long(record)
    return long(record[:-1])


# sync functions
sync_pool = [redis.Redis(host=cof['host'], port=cof['port'], socket_timeout=3) for cof in REDIS_CONFIG]


def get_sub_art(user_id, action=None, fmt=None):
    """
    返回用户关系网内由指定行为推荐的帖子,默认为所有类型
    :param user_id:用户id
    :param action:指定行为,("up", "publish", "inspect ", "comment", "self_pub"),或一系列行为的数组
    :param fmt:帖子类型("v", "g", "i", "w",分别表示视频,gif,图片,文字类型,fmt为类型集合字符串)
    :return:list[aid / dict{action, list[aid
    """
    if isinstance(action, str):     # 获取指定行为
        if action in actions:
            result = sync_pool[long(user_id) % 2].hkeys(PREFIX + "F:" + str(user_id) + ":" + str(action))
            return [_get_art_id(i) for i in result if _check_type(i, fmt)] if result else []
        else:
            return None
    elif (action is None) or isinstance(action, list):     # 获取一组或全部行为
        action = action if action else list(actions)
        action = [a for a in action if a in actions]
        pipe = sync_pool[long(user_id) % 2].pipeline()
        [pipe.hkeys(PREFIX + "F:" + str(user_id) + ":" + str(a)) for a in action]
        results = pipe.execute()
        arts = dict()
        for i in range(len(action)):
            arts[action[i]] = [_get_art_id(_id) for _id in results[i] if _check_type(_id, fmt)] if results[i] else []
        return arts
    else:
        return None


def get_sub_actor(user_id, art_id, action=None):
    """
    获取用户关系网内对指定帖子有指定操作的用户id
    :param user_id: 用户id
    :param art_id: 帖子id或帖子id列表
    :param action: 行为类型("up", "publish", "inspect", "comment", "self_pub"),或一系列行为的数组
    :return: list[uid
    """
    pipe = sync_pool[long(user_id) % 2].pipeline()
    aids = [long(art_id)] if isinstance(art_id, int) or isinstance(art_id, str) or isinstance(art_id, long) else list(art_id)
    acts = [action] if isinstance(action, str) else ([a for a in action if a in actions] if action else list(actions))
    datas = dict()
    for aid in aids:                                                                                      # 帖子
        for act in acts:                                                                                         # 操作
            pipe.smembers(PREFIX + "S:" + str(aid) + ":" + str(user_id) + ":" + str(act))                          # 操作
    results = pipe.execute()
    if isinstance(art_id, int) or isinstance(art_id, str) or isinstance(art_id, long):
        if isinstance(action, str):
            return [_get_data(action, data) for data in results[0]]
        elif (action is None) or isinstance(action, list):
            for i in range(len(acts)):
                datas[acts[i]] = [_get_data(acts[i], data) for data in results[i]] if results[i] else []
    elif isinstance(art_id, list):
        if isinstance(action, str):
            for m in range(len(aids)):
                datas[aids[m]] = [_get_data(action, data) for data in results[m]] if results[m] else []
        elif (action is None) or isinstance(action, list):
            for m in range(len(aids)):
                i_datas = dict()
                for n in range(len(acts)):
                    i_datas[acts[n]] = [_get_data(acts[n], data) for data in results[m * len(acts) + n]] if results[m * len(acts) + n] else []
                datas[aids[m]] = i_datas
        else:
            return None
    else:
        return None
    return datas
# end


if __name__ == "__main__":
    print get_sub_art(11010010101)
    print get_sub_art(11010010101, fmt="v")
    print get_sub_art(11010010101, fmt="w")
    print get_sub_art(11010010101, fmt="wv")
    print get_sub_actor(11010010101, 117901456)
