#coding=utf-8

import time
import json
from im_cli import im_system_msg
from new_im_cli import str_msgid_generate, send_p2p_msg, _create_mysql_push_msg


"""
type: 30,
data: {                # data类型是dict, 不是字符串
	right_url: string, # 必选，完整http url
	right_btn: string, # 必选, 右边按钮文案
	title: string,     # 必选
	icon: string       # 必选，完整http url
	left_url: string,  # 可选，完整http url # NOTE: 安卓将此两个字段处理为可选, iOS认为此两个字段是必选
	left_btn: string,  # 可选, 左边按钮文案

	action: string     # 可选，目前的有效值: 'prize'中奖
	custom: dict       # 可选，此dict中包含的字段，根据action字段值的不同而异
}

    action: 'prize',
    custom: {
        prize_id: string,   # 期号
        content: string     # 奖品文案
    }
"""
def send_type30_msg(fromid, fromnick, fromicon, toid, title, icon, right_url, right_btn, left_url=None, left_btn='查看详情', sub_type=None, action=None, custom=None, push=True):
    data = {
        'title': title,
        'icon': icon,
        'right_url': right_url,
        'right_btn': right_btn
    }
    if left_url:
        data['left_url'] = left_url
        data['left_btn'] = left_btn
    if action:
        data['action'] = action
    if sub_type:  # 用于: 供iOS客户端识别后, 在通知栏顶部做一个滚动栏通知; 安卓目前不会使用此字段
        data['sub_type'] = sub_type
    if custom:
        data['custom'] = custom
    return im_system_msg(fromid, toid, data, 30, fromnick, fromicon, ut=1, push=push)


# 糗商城 - 中奖通知
def send_got_prize_msg(toid, title, icon, right_url, left_url, prize_id, prize_url, prize_content):
    fromid, fromnick, fromicon = 32879940, '糗商城', '2016112418432951.JPEG'
    action, custom = 'prize', {'prize_id': prize_id, 'url': prize_url, 'content': prize_content}
    send_type30_msg(fromid, fromnick, fromicon, toid, title, icon, right_url, '继续参与', left_url, '查看详情', 'prize', action, custom)


# 糗商城 - 即将开奖通知
def send_comming_prize_msg(toid, title, icon, right_url, left_url=None):
    fromid, fromnick, fromicon = 32879940, '糗商城', '2016112418432951.JPEG'
    send_type30_msg(fromid, fromnick, fromicon, toid, title, icon, right_url, '继续参与', left_url, '查看详情', 'comming_prize')


# 使用 "坨坨"官号 或 其他指定帐号 发送文本消息
def send_normal_msg(toid, cnt, fromid=None, fromnick=None, fromicon=None):
    if not fromid:
        fromid, fromnick, fromicon = 21481189, "坨坨", "20150202122548.jpg"
    im_msg = {
        "type": 1,
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "to": toid,
        "msgid": str_msgid_generate(fromid, toid),
        "status": 1,
        "time": int(time.time() * 1000),
        "data": cnt,
    }
    send_p2p_msg(im_msg)
    return im_msg


# 使用 "坨坨"官号 或 其他指定帐号 发送图片消息
def send_pic_msg(toid, pic_url, width, height, fromid=21481189, fromnick='坨坨', fromicon='20150202122548.jpg'):
    im_msg = {
        "type": 3,
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "to": toid,
        "msgid": str_msgid_generate(fromid, toid),
        "status": 1,
        "time": int(time.time() * 1000),
        "data": json.dumps({'url': pic_url, 'height': height, 'width': width}),
    }
    send_p2p_msg(im_msg)
    return im_msg


# 生成 "官号推送" 实体(MySQL)消息
def create_official_immsg(fromid, fromnick, fromicon, data):
    im_msg = {
        "type": 10,  # 官号消息类型
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        # "to": toid,   # NOTE: 发送给用户前, 再针对具体哪个用户, 加上此to字段
        "msgid": str_msgid_generate(fromid, 0),
        "status": 1,
        "time": int(time.time() * 1000),
        "data": data,
        "usertype": 1,  # 官号
        "seq_type": 'push'
    }
    push_id = _create_mysql_push_msg(im_msg)
    return push_id, im_msg


def test_send_normal_msg():
    # 坨坨发纯文本 - 测试
    send_normal_msg(4065020, '恭喜您中奖，获得一张坨坨券')
    send_normal_msg(23643645, '恭喜您中奖，获得一张坨坨券')


# 单图文推送 - 测试
def test_send_official_msg():
    from new_im_cli import batch_send_msg
    fromid, fromnick, fromicon = 24929200, "糗事精选", "20150204102641.jpg"  # push_id: '1594'
    # fromid, fromnick, fromicon = 32879940, '糗商城', '2016112418432951.JPEG'  # push_id: '12340001'

    # push_id: 1594  -->  official_msg_rdb0.get('official:push:1594'), action: 'open'
    # push_id: 12340001 --> 把push_id:1594的消息中的action字段改成'duobao'
    push_dumped_data = '{"title": "\\u4f60\\u62e5\\u6709\\u7684\\u592a\\u5c11\\uff0c\\u4e5f\\u8bb8\\u662f\\u56e0\\u4e3a\\u4f60\\u5403\\u7684\\u592a\\u591a...", "url": "http://public.qiushibaike.com/static/html/24929200/201610/20161025115827038181.html", "msg_id": "1441", "cover": "http://qiubai-im.qiushibaike.com/20161025115827038181", "brief": "\\u4eca\\u5929\\u7b2c\\u4e00\\u5929\\u51cf\\u80a5\\uff0c\\u98df\\u8c31\\u4e0a\\u8bf4\\u53ea\\u80fd\\u716e\\u70b9\\u9752\\u83dc\\u5403\\u3002\\u4e8e\\u662f\\u6211\\u5c31\\u4e70\\u4e86\\u70b9\\u9752\\u83dc\\u716e\\uff0c\\u767d\\u6c34\\u716e\\u751f\\u83dc\\u89c9\\u5f97\\u4e0d\\u597d\\u5403\\uff0c\\u4e8e\\u662f\\u5c31\\u52a0\\u4e86\\u70b9\\u76d0\\uff0c\\u6253\\u5f00\\u51b0\\u7bb1\\u90fd\\u662f\\u9752\\u83dc\\uff0c\\u53c8...", "action": "open", "post_ids": ["117824549", "117824746", "117829836", "117825540", "117830123", "117824165", "117824537", "117826147", "117825618", "117825839"]}'
    push_id, im_msg = create_official_immsg(fromid, fromnick, fromicon, push_dumped_data)
    batch_send_msg(push_id, im_msg, [4065020, 23643645])


# 多图文推送 - 测试
def test_send_official_msg2():
    from new_im_cli import batch_send_msg
    fromid, fromnick, fromicon = 24609252, "糗百货", "20150204102840.jpg"  # push_id: '12340001'
    #fromid, fromnick, fromicon = 32879940, '糗商城', '2016112418432951.JPEG'  # push_id: '12340002'

    # push_id: 742  -->  official_msg_rdb0.get('official:push:742'), action: 'open'
    # push_id: 12340002 --> 把push_id:742的消息, 添加action字段值'duobao'
    push_dumped_data = '{"url": "http://www.wemart.cn/v2/weimao/index.html?disableCache=true&qpaper=1029mad6&shopId=shop001201501095297#mad/shop001201501095297/6&qiushibaikerole=qiubaihuo", "title": "\\u4f60\\u5988\\u903c\\u4f60\\u7a7f\\u79cb\\u88e4\\u4e86\\u5417\\uff1f", "msg_id": "564", "cover": "http://qiubai-im.qiushibaike.com/20151029165258713407", "others": [{"public_id": 24609252, "title": "\\u30103\\u6298\\u3011\\u4e0d\\u5fc5\\u7b49\\u5230\\u53cc11\\uff01\\u63d0\\u524d\\u64b8\\uff01\\u5168\\u573a3\\u6298\\uff01", "url": "http://www.wemart.cn/v2/weimao/index.html?disableCache=true&qpaper=1029mad6&shopId=shop001201501095297#mad/shop001201501095297/6&qiushibaikerole=qiubaihuo", "msg_id": "566", "cover": "http://qiubai-im.qiushibaike.com/20151029172048662654"}, {"public_id": 24609252, "title": "\\u3010\\u62a2\\u7ea2\\u5305\\u3011\\u7ea2\\u5305\\u5012\\u8ba1\\u65f6\\uff0c\\u8fd8\\u5269500\\u4efd\\uff0c\\u62a2\\uff01", "url": "http://www.wemart.cn/v2/weimao/index.html?qpaper=1029gc6&shopId=shop001201501095297&wmode=app#gc/1122/6&qiushibaikerole=qiubaihuo", "msg_id": "565", "cover": "http://qiubai-im.qiushibaike.com/20151029172002544751"}]}'
    push_id, im_msg = create_official_immsg(fromid, fromnick, fromicon, push_dumped_data)
    batch_send_msg(push_id, im_msg, [4065020, 23643645])


def test_send_got_prize_msg():
    toid = 19920461
    title = '恭喜你在糗商城中获得「巴宝莉经典流苏羊绒围巾」，快来看看吧！'
    icon = 'http://ww2.sinaimg.cn/large/65e4f1e6jw1f9bgvnifjgj205l05k747.jpg'
    prize_url = left_url = 'http://bao.qiushibaike.com/goods/2'
    right_url = 'http://bao.qiushibaike.com/goods/2'
    prize_id, prize_content = '12345678', '巴宝莉经典流苏羊绒围巾'
    send_got_prize_msg(toid, title, icon, right_url, left_url, prize_id, prize_url, prize_content)


def test_send_comming_prize_msg():
    toid = 19920461
    title = '你参与的「巴宝莉经典流苏羊绒围巾」即将揭晓，见证幸运的时刻来了！'
    icon = 'http://ww2.sinaimg.cn/large/65e4f1e6jw1f9bgvnifjgj205l05k747.jpg'
    left_url = 'http://bao.qiushibaike.com/home'
    right_url = 'http://bao.qiushibaike.com/home'
    send_comming_prize_msg(toid, title, icon, right_url, left_url)
    #send_comming_prize_msg(toid, title, icon, right_url)


if __name__ == '__main__':
    pass
    #send_normal_msg(4065020, 'abcd')
    #send_normal_msg(4065020, 'abcd', fromid=32879940, fromnick='糗商城', fromicon='2016112418432951.JPEG')
    #test_send_got_prize_msg()
    #test_send_comming_prize_msg()
    #test_send_official_msg()
    #test_send_official_msg2()
    #send_pic_msg(23643645, 'https://qiubai-im.qiushibaike.com/Ft3_mA6FarMgC3vOfSbnJgjdVZhw', 1280, 960)
    #send_pic_msg(23643645, 'https://qiubai-im.qiushibaike.com/Ft3_mA6FarMgC3vOfSbnJgjdVZhw', 640, 480)

