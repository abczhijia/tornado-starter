# coding=utf-8

import base64
from Crypto.Cipher import AES
from Crypto import Random


BS = AES.block_size  # aes数据分组长度为128 bit
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)  # PKCS7 padding算法
unpad = lambda s: s[0:-ord(s[-1])]


class AesTool(object):

    def __init__(self, key, mode):
        self.key = key
        self.mode = mode

    def encrypt(self, plaintext):
        iv = Random.new().read(BS)  # 生成随机初始向量IV
        cryptor = AES.new(self.key, self.mode, iv)
        cipher_text = cryptor.encrypt(pad(plaintext))
        return base64.standard_b64encode(iv + cipher_text)

    def decrypt(self, base64_text):
        cipher_text = base64.standard_b64decode(base64_text)
        iv = cipher_text[:BS]
        cipher_text = cipher_text[BS:]
        cryptor = AES.new(self.key, self.mode, iv)
        plaintext = cryptor.decrypt(cipher_text)
        return unpad(plaintext)


g_aes_tool = AesTool(b'2Gj1hM3gbWX10qaTzpE4HNGKtX5gwvgv', AES.MODE_CBC)

