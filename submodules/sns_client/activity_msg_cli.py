#coding=utf-8

import time
import logging

from new_im_cli import str_msgid_generate, send_p2p_msg, _create_mysql_push_msg


# type: 32 暂时定为'活动类型消息'; 以后需要新增一些生命周期较短的消息类型, 都可以考虑放到此类型下, 新增子类型, 即data字段里的t字段
def send_type_32_msg(toid, data, fromid=21481189, fromnick='坨坨', fromicon='20150202122548.jpg', custom=None, push=True):
    im_msg = {
        "type": 32,
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "to": toid,
        "msgid": str_msgid_generate(fromid, toid),
        "status": 1,
        "time": int(time.time() * 1000),
        "notify": True,
        "data": data,
    }
    if custom:
        im_msg.update(custom)
    if push:
        send_p2p_msg(im_msg)
    return im_msg


"""
data: {
	t: 'qsjx_hammer',     # 必选
	title: string,        # 必选
	sub_title: string,    # 可选，当且仅当gold_hammer和gold_hammer都不为0时，才含有此字段
	content: string,      # 必选
	gold_hammer: int,     # 必选，金锤子数量，0代表没有
	color_hammer: int,    # 必选，彩锤子数量，0代表没有
	btn_cnt: string,      # 必选，按钮文案
	btn_url: string,      # 必选，按钮跳转URL
}

# 客户端收到后需要弹窗, 每个用户至多发一次(活动期间有登录的用户才会发)
"""
def send_qsjx_hammer_msg(toid, qsjx_count):
    if int(qsjx_count) > 0:
        title = '恭喜您获得了%s个砸金蛋锤子' % qsjx_count
        content = '亲爱的糗友，新年快乐，在2016年，您获得了 %s 个精选，现赠送您 %s 个砸金蛋锤子，以资奖励，希望您在2017年继续给大家分享糗事，祝您新年中大奖！' % (qsjx_count, qsjx_count)
        sub_title = '登录获得砸彩蛋锤子1个'
        gold_hammer = qsjx_count
        old_data = content + '\n请升级至最新版参加活动哦!'
    else:
        title = '恭喜获得砸彩蛋锤子1个'
        content = '活动期间，每天登录都送砸彩蛋锤子哦'
        sub_title = None
        gold_hammer = 0
        old_data = "%s！%s%s" % (title, content, '\n请升级至最新版参加砸蛋活动!')

    data = {
        't': 'qsjx_hammer',
        'title': title,
        'content': content,
        'gold_hammer': gold_hammer,
        'color_hammer': 1,
        'btn_cnt': '去砸蛋>>',
        'btn_url': 'http://act.qiushibaike.com/2017/index'
    }
    if sub_title:
        data['sub_title'] = sub_title

    custom = {
        'min_av': 100800,
        'min_iv': 100800,
        'min_replace_keys': {
            'type': 1,
            'data': old_data
        }
    }
    send_type_32_msg(toid, data, custom=custom)
    logging.info(u'qsjx_hammer, to:%s, qsjx:%s', toid, qsjx_count)


"""
data: {
	t: 'hammer',        # 必选
	title: string,      # 必选
	icon_url: string,   # 必选, 左图URL
	btn_cnt: string,    # 必选，按钮文案
	btn_url: string,    # 必选，按钮跳转URL
}

# 用于:

A. 发送获得新锤子的通知
   文案: 恭喜获得xx个砸金蛋锤子

B. 21:00提醒用户使用锤子, 否则0点过期
   文案:
   你还有x个砸金蛋锤子，和x个砸彩蛋锤子，
   请尽快使用，0点将全部清空

   你还有x个砸金蛋锤子
   请尽快使用，0点将全部清空

   你还有x个砸彩蛋锤子
   请尽快使用，0点将全部清空
"""
def send_hammer_msg(toid, content, push=True, btn_url="http://act.qiushibaike.com/2017/index", btn_cnt='去砸蛋>>', icon_url='http://qiubai-bao.qiushibaike.com/q1v98.png'):
    data = {
        't': 'hammer',
        'title': content,
        'icon_url': icon_url,
        'btn_url': btn_url,
        'btn_cnt': btn_cnt,
    }
    custom = {
        'min_av': 100800,
        'min_iv': 100800,
        'min_replace_keys': {
            'type': 1,
            'data': content + '\n请升级至最新版参加砸蛋活动!'
        }
    }
    im_msg = send_type_32_msg(toid, data, custom=custom, push=push)
    if push:
        logging.info(u'hammer, to:%s, content:%s', toid, content)
    return im_msg


def create_hammer_immsg(content, btn_url="http://act.qiushibaike.com/2017/index", btn_cnt='去砸蛋>>', icon_url='http://qiubai-bao.qiushibaike.com/q1v98.png'):
    im_msg = send_hammer_msg(0, content, False, btn_url, btn_cnt, icon_url)
    im_msg.pop('to', None)
    push_id = _create_mysql_push_msg(im_msg)
    return push_id, im_msg


def cron_send_test():
    import sys
    user_id = int(sys.argv[1])
    while 1:
        send_qsjx_hammer_msg(user_id, 2)
        send_qsjx_hammer_msg(user_id, 0)
        send_hammer_msg(user_id, '恭喜获得1个砸金蛋锤子', 'http://act.qiushibaike.com/2017/index')
        time.sleep(300)
    #send_hammer_msg(4065020, '你还有1个砸彩蛋锤子\n请尽快使用，0点将全部清空', 'http://act.qiushibaike.com/2017/index')


if __name__ == '__main__':
    pass
    #cron_send_test()

    # 测试批量发
    #from new_im_cli import batch_send_msg
    #push_id, im_msg = create_hammer_immsg('恭喜获得1个砸金蛋锤子')
    #batch_send_msg(push_id, im_msg, [4065020, 23643645])

    #uids = [4065020, ] #32835296, 16841639, 30755821, 31405910, 31522850, 16824217, 16846253, 23900910, 16841639, 16846109, 32238553]
    #for uid in uids:
    #    send_qsjx_hammer_msg(uid, 3)
    #    #send_hammer_msg(uid, '恭喜获得1个砸金蛋锤子', 'http://act.qiushibaike.com/2017/index')



