# -*- coding: utf-8 -*-

import errors
from tornado.log import gen_log
from tornado.web import HTTPError
from tornado.escape import json_encode, json_decode
from udp_stats_cli import Client
from thrift_async_cli import error_ttypes


class RequestMixin(object):

    def on_finish(self):
        if hasattr(self.application, 'enable_statsd') and self.application.enable_statsd\
                and hasattr(self.application, 'statsd_prefix') and self.application.statsd_prefix:
            if not hasattr(self, '_statsd_client'):
                self._statsd_client = Client(host='10.232.76.50', port=8125, prefix=self.application.statsd_prefix)
            bucket = '.'.join([part for part in self.request.path.strip('/').split('/') if not part.isdigit()])
            self._statsd_client.timing_since(bucket, self.request._start_time)

    def get_json_args(self):
        return json_decode(self.request.body) if self.request.body else {}

    def write_json(self, data):
        self.set_header('Content-Type', 'application/json')
        return self.write(data)

    def write_null_page_json(self, data=None):
        if data is None:
            data = {}
        data.update({'total': 0, 'data': [], 'has_more': False})
        return self.write_ok_json(data)

    def write_ok_json(self, data=None):
        if data is None:
            data = {}
        data.update(err=0)
        self.write_json(json_encode(data))
        self.finish()

    def write_fail_json(self, error):
        self.write_json(json_encode({'err': error.code, 'err_msg': error.message}))
        self.finish()

    @property
    def token(self):
        return self.request.headers.get('Qbtoken')

    @property
    def remote_ip(self):
        return self.request.headers['X-Real-Ip'] if 'X-Real-Ip' in self.request.headers else self.request.remote_ip

    @property
    def source(self):
        return self.request.headers.get('Source')

    @property
    def app(self):
        return self.source.split('_')[0] if self.source else None

    @property
    def user_agent(self):
        return self.request.headers.get('User-Agent', '')

    @property
    def version(self):
        return self.get_version(self.source.split('_')[1]) if self.source else None

    @property
    def uuid(self):
        return self.request.headers.get('Uuid')

    @property
    def channel(self):
        return self.user_agent.split('_')[-1] if self.app and self.app == 'android' and self.user_agent else ''

    @property
    def x_app(self):
        # '1': 糗百 '2': Remix '3': 糗百直播
        return self.request.headers.get('App', '1')

    def get_version(self, version):
        ''' version必须是三位'''
        vs = tuple(map(int, version.split('.')))
        return vs + (0, ) * (3 - len(vs))

    def _request_summary(self):
        data = dict()
        data['qbtoken'] = self.request.headers.get('Qbtoken', '')
        data['source'] = self.request.headers.get('Source', '')
        data['mobile_uuid'] = self.request.headers.get('Uuid', '')
        data['ip'] = self.request.headers.get('X-Real-Ip', '')
        data['ua'] = self.request.headers.get('User-Agent', '')
        return self.request.method + ' ' + self.request.uri + ' ' + str(data)

    def log_exception(self, typ, value, tb):
        if isinstance(value, HTTPError):
            if value.log_message:
                args = ([value.status_code, self._request_summary()] + list(value.args))
                gen_log.warning("%d %s: " + value.log_message, *args)
        # 自定义应用错误或者服务错误
        elif isinstance(value, errors.AppError) or isinstance(value, error_ttypes.ServiceException):
            if value.code == error_ttypes.ErrorCode.TOKEN_NOT_EXIST:
                value = errors.ErrTokenInvalid
            if value.code == error_ttypes.ErrorCode.PUNISH_NOT_EXIST:
                value = errors.AppError(err=30009, message=u'你的帐号违规被永久封禁，如有疑问可到意见反馈处申诉')
            self.write_fail_json(value)
        elif hasattr(self.application, 'app_error_log'):
            self.application.app_error_log.error(
                "Uncaught exception %s\n%r", self._request_summary(), self.request, exc_info=(typ, value, tb))
