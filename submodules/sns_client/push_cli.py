#coding=utf-8

import time
import json
import redis
import logging


mq_name = "redismq:push%s" # 优先级: redismq:push0 > redismq:push1 > redismq:push2
g_push_redis = redis.StrictRedis(host='10.251.74.150', port=16379, db=1)


def push_cli(toid, content, what_push, custom, priority):
    msg = {
        'source': 'custom',
        'what_push': what_push,
        'toid': int(toid),
        'cnt': content,
        'custom': custom
    }
    t1 = time.time()
    g_push_redis.lpush(mq_name % priority, json.dumps(msg))
    t2 = time.time()
    logging.info(u'push to %s, cnt:%s, %.1fms', toid, content, (t2 - t1) * 1000)


def batch_push_cli(toids, content, what_push, custom, priority):
    assert isinstance(toids, (tuple, list))
    msg = {
        'source': 'batch_custom',
        'what_push': what_push,
        'cnt': content,
        'custom': custom
    }
    pipe_size = 1000
    lpush_count = (len(toids) + pipe_size - 1) / pipe_size
    for i in xrange(lpush_count):
        page_toids = toids[i * pipe_size: (i + 1) * pipe_size]
        page_size = 25  # 服务端同一分页下顺序处理, 故分页设置小一点, 减少延迟
        page_count = (len(page_toids) + page_size - 1) / page_size
        pipeline = g_push_redis.pipeline(transaction=False)
        t1 = time.time()
        for j in xrange(page_count):
            msg['toids'] = page_toids[j * page_size: (j + 1) * page_size]
            pipeline.lpush(mq_name % priority, json.dumps(msg))
            logging.info(u'push to %s, cnt:%s', toids, content)
        t2 = time.time()
        pipeline.execute()
        logging.info(u'pipeline execute, cost: %.1fms', (t2 - t1) * 1000)


# 糗友圈签到推送
def circle_sign_push(toid, content):
    push_cli(toid, content, 'circle_sign', {'t': 'checkin'}, 1)


def circle_sign_batch_push(toids, content):
    batch_push_cli(toids, content, 'circle_sign', {'t': 'checkin'}, 1)


# 不走IM的糗事精选推送, 点击后显示在'专享Tab页'的顶部, android>=10.5.1
def qsjx_push(toid, qsjx_title, article_ids):
    custom = {
        't': 'qsjx',
        "v": {
            'article_ids': article_ids,
            'title': qsjx_title,
            'date': 1477473307,
            'pic': 'http://qiubai-im.qiushibaike.com/20161026111111369564'
        }
    }
    push_cli(toid, qsjx_title, 'qsjx', custom, 1)


if __name__ == '__main__':
    #circle_sign_push(4065020, '亲, 来签到吧~')
    #circle_sign_batch_push([4065020, 23643645], '亲, 来签到吧~')
    qsjx_push(20638549, '1/10000的迷之巧合，被我给碰上了！',
              [117832564, 117831456, 117832116, 117834327, 117833743, 117833103, 117831540, 117836905, 117836323, 117835908])
