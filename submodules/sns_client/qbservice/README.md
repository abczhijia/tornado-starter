### 糗百内部RPC服务库

#### 1. 部署地址
| 服务 | 部署机器 | 部署路径 | 运行地址 | 环境 |
| ---- | -------- | -------- | -------- | ---- |
| 用户服务 | t-service2    | /home/bot/services/user | 10.232.46.15:18002 | 测试环境 |
| 用户服务 | t-sub-worker1 | /home/bot/services/user | 10.221.236.76:8100 | 正式环境 |
| 用户服务 | t-sub-worker2 | /home/bot/services/user | 10.207.169.67:8100 | 正式环境 |
| 用户服务 | t-sub-worker4 | /home/bot/services/user | 10.232.64.105:8100 | 备份环境 |
| 用户服务 | t-sub-worker5 | /home/bot/services/user | 10.221.53.28:8100 | 备份环境 |

| 评论服务 | t-service2    | /home/bot/services/comment | 10.232.46.15:18003 | 测试环境 |
| 评论服务 | t-sub-worker1 | /home/bot/services/comment | 10.221.236.76:8200 | 正式环境 |
| 评论服务 | t-sub-worker2 | /home/bot/services/comment | 10.207.169.67:8200 | 正式环境 |
| 评论服务 | t-sub-worker4 | /home/bot/services/comment | 10.232.64.105:8200 | 备份环境 |
| 评论服务 | t-sub-worker5 | /home/bot/services/comment | 10.221.53.28:8200 | 备份环境 |

| 帖子服务 | t-service2    | /home/bot/services/article | 10.232.46.15:18004 | 测试环境 |
| 帖子服务 | t-sub-worker1 | /home/bot/services/article | 10.221.236.76:9711-9740 | 正式环境 |
| 帖子服务 | t-sub-worker2 | /home/bot/services/article | 10.207.169.67:9711-9740 | 正式环境 |
| 帖子服务 | t-sub-worker4 | /home/bot/services/article | 10.232.64.105:9711-9740 | 正式环境 |
| 帖子服务 | t-sub-worker5 | /home/bot/services/article | 10.221.53.28:9711-9740 | 正式环境 |

#### 2. 用户/评论/帖子服务使用方法
- 异步（依赖sns_client.thrift_async_cli）：
```python
from submodules.sns_client import thrift_async_cli
from submodules.sns_client.thrift_async_cli import error_ttypes

_user_client = thrift_async_cli.get_async_user_client()
_comment_client = thrift_async_cli.get_async_comment_client()
_article_client = thrift_async_cli.get_async_article_client()

try:
    _user_client.get_user_by_login(login.encode('utf-8'))
except error_ttypes.ServiceException, se:
    if se.code != error_ttypes.ErrorCode.USER_NOT_EXIST:
        raise
```

- 同步（依赖sns_client.thrift_sync_cli）
```python
from submodules.sns_client.thrift_sync_cli import _sync_safe_execute_user

_sync_safe_execute_user("set_user_avatar", image['id'], image['filename'], image['sp'])
```
