include "errors.thrift"

namespace go services.articles
namespace py services.articles

enum TopicCounterType {
    SUB,
    VIEW,
    ARTICLE
}

struct CreateArticleRequest {
    1: required i64 user_id
    2: required string tag_line
    3: required string content
    4: required string created_at
    5: required string updated_at
    6: required string picture_file_name
    7: required string picture_content_type
    8: required string picture_updated_at
    9: required string source
    10: required string status
    11: required string email
    12: required string user_agent
    13: required i32 anonymous
    14: required i32 picture_file_size
    15: required i64 ip
    16: optional i64 topic_id
}

struct UpdateArticlePictureNameRequest{
    1: required i64 id
    2: required i64 user_id
    3: required string picture_file_name
}

struct UpdateArticleStatusRequest{
    1: required i64 id
    2: required string status
    3: optional i64 article_user_id
    4: optional list<string> pre_status
}

struct UpdateArticleRequest{
    1:required i64 id
    2:optional i64 article_user_id
    3:optional string picture_file_name
    4:optional string article_type
    5:optional string picture_content_type
    6:optional string similar
    7:optional string comment_status
    8:optional string tag_line
    9:optional string content
    10:optional string title
    11:optional i32 anonymous
    12:optional string keywords
}


struct GetMyArticlesRequest {
    1: required i64 user_id
    2: required bool permit_video
    3: optional i32 offset = 0
    4: optional i32 limit = 10
}

struct GetOtherArticlesRequest{
    1: required i64 user_id
    2: required bool permit_video
    3: optional i32 offset = 0
    4: optional i32 limit = 10
}

struct GetCollectArticlesRequest{
    1: required i64 user_id
    2: required bool permit_video
    3: optional i32 offset = 0
    4: optional i32 limit = 10
}

struct DeleteArticleRequest {
    1: required i64 id
    2: optional i64 article_user_id
}

struct GetMyArticlesCountRequest {
    1: required i64 user_id
    2: required bool permit_video
}

struct GetOtherArticlesCountRequest{
    1: required i64 user_id
    2: required bool permit_video
}

struct GetCollectArticlesCountRequest{
    1: required i64 user_id
    2: required bool permit_video
}

struct Vote {
    1: required i32 up
    2: required i32 down
}

struct TopicCounter {
    1: required i64 sub_count
    2: required i64 view_count
    3: required i64 article_count
}

struct Topic {
    1: required i64 id
    2: required string content
    3: required string avatar
    4: required string background
    5: required i32 status
    6: optional TopicCounter counter
}

struct Article {
    1: required i64 id
    2: required i64 user_id
    3: required string tag_line
    4: required string content
    5: required string created_at
    6: required string status
    7: required string comment_status
    8: required string picture_file_name
    9: required string picture_content_type
    10:required string title
    11: required i64 anonymous
    12: required i64 comments_count
    13: required i64 published_at
    14: required Vote votes
    15: optional i64 share_count
    16: optional i64 loop
    17: optional string keywords
    18: optional string image_size
    19: optional string email
    20: optional Topic topic
}

struct ListRequest{
    1: required string format
    2: required i32 page
    3: required i32 group_id
    4: required bool permit_video
    5: optional i32 limit = 30
    6: optional i32 list_page = 720
}

struct InsertImageRelatedRequest{
    1:required i64 id
    2:required i64 user_id
    3:required string uuid
    4:required string source
    5:required i32 image_width
    6:required i32 image_height
    7:required string image_type
    8:required i32 screen_width
    9:required i32 screen_height
}

struct InsertScoresRequest{
    1:required i64 article_id
    2:required i64 neg
    3:required i64 pos
    4:required i64 public_comments_count
    5:required i64 public_references_count
    6:required i64 score
    7:required i64 ratings_count
    8:required i32 group_id
    9:required string created_at
    10:required i32 has_picture
    11:required string status
    12:required i64 wneg
    13:required i64 wpos
    14:optional i64 article_user_id
}


struct UpdateArticleScoresRequest{
    1:required i64 article_id
    2:optional i64 neg
    3:optional i64 pos
    4:optional i64 score
    5:optional i64 article_user_id
    6:optional i64 public_comments_count
    7:optional i64 public_references_count
    8:optional i64 group_id
    9:optional i64 ratings_count
    10:optional i32 has_picture
    11:optional string created_at

}


struct AddVideoLoopRequest {
    1:required i64 article_id
    2:required i64 loop_count
}

struct ArticleSharedRequest {
    1:required i64 user_id
    2:required i64 article_id
    3:required string channel
    4:required string platform
}

struct IncreScoresRequest {
    1:required i64 article_id
    2:required i64 new_pos
    3:required i64 new_neg
    4:optional i64 article_user_id
}

struct ArticlePageResult {
    1:required i64 total
    2:required list<Article> articles
}

struct TopicPageResult {
    1:required i64 total
    2:required list<Topic> topics
}

service ArticleService {
    i64 create_article(1:CreateArticleRequest request) throws (1:errors.ServiceException se)
    bool update_article_picture_name(1:UpdateArticlePictureNameRequest request) throws (1:errors.ServiceException se)
    bool update_article_status(1:UpdateArticleStatusRequest request) throws (1:errors.ServiceException se)
    bool delete_article(1:DeleteArticleRequest request) throws (1:errors.ServiceException se)
    list<Article> get_my_articles(1:GetMyArticlesRequest request) throws (1:errors.ServiceException se)
    list<Article> get_other_articles(1:GetOtherArticlesRequest request) throws (1:errors.ServiceException se)
    list<Article> get_collect_articles(1:GetCollectArticlesRequest request) throws (1:errors.ServiceException se)
    i32 get_my_articles_count(1:i64 user_id,2:bool permit_video) throws (1:errors.ServiceException se)
    i32 get_other_articles_count(1:i64 user_id,2:bool permit_video) throws (1:errors.ServiceException se)
    i32 get_collect_articles_count(1:i64 user_id,2:bool permit_video) throws (1:errors.ServiceException se)
    Article get_article_by_id(1:i64 article_id) throws (1:errors.ServiceException se)
    map<i64, Article> get_articles_by_ids(1:list<i64> article_ids) throws (1:errors.ServiceException se)
    list<Article> get_mult_articles_by_ids(1:list<i64> article_list,2:bool permit_video,3:list<string> status_list) throws (1:errors.ServiceException se)
    i64 get_user_id_by_art_id(1:i64 id) throws (1:errors.ServiceException se)
    bool add_collect_article(1:i64 user_id, 2:i64 article_id, 3:bool permit_video) throws (1:errors.ServiceException se)
    bool delete_collect_article(1:i64 user_id, 2:i64 article_id, 3:bool permit_video) throws (1:errors.ServiceException se)
    bool insert_image_related(1:InsertImageRelatedRequest request) throws (1:errors.ServiceException se)
    bool insert_scores(1:InsertScoresRequest request) throws (1:errors.ServiceException se)
    bool update_article_scores(1:UpdateArticleScoresRequest request) throws (1:errors.ServiceException se)
    bool update_article(1:UpdateArticleRequest request) throws (1:errors.ServiceException se)
    map<i64, string> get_articles_titles_by_ids(1:list<i64> article_list) throws (1:errors.ServiceException se)
    void add_video_loop(1:AddVideoLoopRequest request) throws (1:errors.ServiceException se)
    bool create_article_share(1:ArticleSharedRequest request) throws (1:errors.ServiceException se)
    void incre_scores(1:IncreScoresRequest request) throws (1:errors.ServiceException se)
    void forbid_user_articles(1:i64 user_id) throws(1:errors.ServiceException se)
    i64 get_user_smile_face(1:i64 user_id,2:bool permit_video) throws (1:errors.ServiceException se)

    void subscribe_topic(1:i64 user_id, 2:i64 topic_id) throws (1:errors.ServiceException se)
    void unsubscribe_topic(1:i64 user_id, 2:i64 topic_id) throws (1:errors.ServiceException se)
    map<i64, Topic> get_topics_by_ids(1:list<i64> topic_ids) throws (1:errors.ServiceException se)
    map<i64, bool> check_topic_subed(1:i64 user_id, 2:list<i64>topic_ids) throws (1:errors.ServiceException se)
    TopicPageResult get_all_topics(1:i32 offset, 2:i32 limit) throws (1:errors.ServiceException se)
    TopicPageResult get_user_topics(1:i64 user_id, 2:i32 offset, 3:i32 limit) throws (1:errors.ServiceException se)
    ArticlePageResult get_topic_articles(1:i64 topic_id, 2:i32 offset, 3:i32 limit) throws (1:errors.ServiceException se)
    void update_topic_counter(1:i64 topic_id, 2:TopicCounterType counter 3:i16 change, 4:bool is_increase) throws (1:errors.ServiceException se)
}
