include "page.thrift"
include "errors.thrift"

namespace go services.devices
namespace py services.devices

struct Device {
    1: optional i64 id = 0        // 注意: 可选字段若是没有默认值, 生成的go代码里, 此字段的类型会是 *int64
    2: required i64 user_id
    3: required i64 app_version
    4: required i64 enable_push
    5: required i64 updated_at
    6: required i64 token_type
    7: required string push_token
    8: required string device_id
    9: optional bool exist = true  // 当且仅当此字段为true时, 其他字段才有意义
}

struct AppVersion {
    1: required i64 app_version   // 100301 --> 10.3.1
    2: required string type       // 有效值: 'android', 'ios'
    3: optional bool exist = true // 当且仅当此字段为true时, 其他字段才有意义
}

struct ExecResult {  // 注意: 生成的go代码里, 类型变成 ExecResult_
    1: required bool success
    2: required i64 affected  // update, create, delete 执行后影响到的行数
}

service DeviceService {
    map<i64, Device> get_devices_by_userids(1:list<i64> user_ids) throws (1:errors.ServiceException se)
    Device get_device_by_userid(1:i64 user_id) throws (1:errors.ServiceException se)
    Device get_device_by_deviceid(1:string device_id) throws (1:errors.ServiceException se)

    ExecResult update_latest_app_version(1:i64 user_id 2:AppVersion app_version) throws (1:errors.ServiceException se)
    ExecResult update_device_by_userid(1:Device device) throws (1:errors.ServiceException se)
    ExecResult update_device_by_deviceid(1:Device device) throws (1:errors.ServiceException se)

    ExecResult delete_device_by_pushtoken(1:string push_token) throws (1:errors.ServiceException se)
    ExecResult delete_device_by_userid(1:i64 user_id) throws (1:errors.ServiceException se)
    ExecResult delete_device_by_deviceid(1:string device_id) throws (1:errors.ServiceException se)

    AppVersion get_latest_app_version(1:i64 user_id) throws (1:errors.ServiceException se)
    ExecResult create_device(1:Device device) throws (1:errors.ServiceException se)
}
