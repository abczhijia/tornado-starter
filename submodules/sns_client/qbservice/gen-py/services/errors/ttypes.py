#
# Autogenerated by Thrift Compiler (0.9.3)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py
#

from thrift.Thrift import TType, TMessageType, TException, TApplicationException

from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol, TProtocol
try:
  from thrift.protocol import fastbinary
except:
  fastbinary = None


class ErrorCode:
  DATABASE_ERROR = 10001
  REDIS_ERROR = 10002
  MEMCACHE_ERROR = 10003
  SERIALIZATION_ERROR = 10004
  QUEYR_COUNT_OVERFLOW = 10005
  PASSWORD_WRONG = 20001
  USER_NOT_EXIST = 20002
  TOKEN_NOT_EXIST = 20003
  THIRD_ACCOUNT_NOT_EXIST = 20004
  AVATAR_HISTORY_NOT_EXIST = 20005
  EMAIL_BIND_NOT_EXIST = 20006
  VISIT_NOT_EXIST = 20007
  NICK_NOT_EXIST = 20008
  PUSH_BIND_NOT_EXIST = 20009
  PUNISH_NOT_EXIST = 20010
  COMMENT_NOT_EXIST = 40001
  ARTICLE_NOT_EXIST = 50001
  ARTICLE_SERVER_ERROR = 50002
  INPUT_INVALID_ERROR = 90000

  _VALUES_TO_NAMES = {
    10001: "DATABASE_ERROR",
    10002: "REDIS_ERROR",
    10003: "MEMCACHE_ERROR",
    10004: "SERIALIZATION_ERROR",
    10005: "QUEYR_COUNT_OVERFLOW",
    20001: "PASSWORD_WRONG",
    20002: "USER_NOT_EXIST",
    20003: "TOKEN_NOT_EXIST",
    20004: "THIRD_ACCOUNT_NOT_EXIST",
    20005: "AVATAR_HISTORY_NOT_EXIST",
    20006: "EMAIL_BIND_NOT_EXIST",
    20007: "VISIT_NOT_EXIST",
    20008: "NICK_NOT_EXIST",
    20009: "PUSH_BIND_NOT_EXIST",
    20010: "PUNISH_NOT_EXIST",
    40001: "COMMENT_NOT_EXIST",
    50001: "ARTICLE_NOT_EXIST",
    50002: "ARTICLE_SERVER_ERROR",
    90000: "INPUT_INVALID_ERROR",
  }

  _NAMES_TO_VALUES = {
    "DATABASE_ERROR": 10001,
    "REDIS_ERROR": 10002,
    "MEMCACHE_ERROR": 10003,
    "SERIALIZATION_ERROR": 10004,
    "QUEYR_COUNT_OVERFLOW": 10005,
    "PASSWORD_WRONG": 20001,
    "USER_NOT_EXIST": 20002,
    "TOKEN_NOT_EXIST": 20003,
    "THIRD_ACCOUNT_NOT_EXIST": 20004,
    "AVATAR_HISTORY_NOT_EXIST": 20005,
    "EMAIL_BIND_NOT_EXIST": 20006,
    "VISIT_NOT_EXIST": 20007,
    "NICK_NOT_EXIST": 20008,
    "PUSH_BIND_NOT_EXIST": 20009,
    "PUNISH_NOT_EXIST": 20010,
    "COMMENT_NOT_EXIST": 40001,
    "ARTICLE_NOT_EXIST": 50001,
    "ARTICLE_SERVER_ERROR": 50002,
    "INPUT_INVALID_ERROR": 90000,
  }


class ServiceException(TException):
  """
  Attributes:
   - code
   - message
  """

  thrift_spec = (
    None, # 0
    (1, TType.I32, 'code', None, None, ), # 1
    (2, TType.STRING, 'message', None, None, ), # 2
  )

  def __init__(self, code=None, message=None,):
    self.code = code
    self.message = message

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.I32:
          self.code = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.STRING:
          self.message = iprot.readString()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('ServiceException')
    if self.code is not None:
      oprot.writeFieldBegin('code', TType.I32, 1)
      oprot.writeI32(self.code)
      oprot.writeFieldEnd()
    if self.message is not None:
      oprot.writeFieldBegin('message', TType.STRING, 2)
      oprot.writeString(self.message)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    if self.code is None:
      raise TProtocol.TProtocolException(message='Required field code is unset!')
    if self.message is None:
      raise TProtocol.TProtocolException(message='Required field message is unset!')
    return


  def __str__(self):
    return repr(self)

  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.code)
    value = (value * 31) ^ hash(self.message)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, value)
      for key, value in self.__dict__.iteritems()]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

  def __ne__(self, other):
    return not (self == other)
