include "page.thrift"
include "errors.thrift"

namespace go services.comments
namespace py services.comments


struct CreateCommentRequest {
    1: required i64 user_id
    2: required i64 article_id
    3: required string content
    4: required string source
    5: required i32 anonymous
    6: required string user_ip
    7: optional i64 parent_id
    8: optional map<string, i64> refers
}

struct DeleteCommentRequest {
    1: required i64 user_id
    2: required i64 comment_id
    3: required i64 article_id
}

struct Comment {
    1: required i64 id
    2: required i64 article_id
    3: required i64 user_id
    4: required string user_ip
    5: required i32 anonymous
    6: required string content
    7: required string status
    8: required string source
    9: required string created_at
    10: required i32 pos
    11: required i32 neg
    12: required i32 floor
    13: required i32 score
    14: required i64 parent_id
    15: required i32 like_count
    16: optional map<string, i64> refers
}

service CommentService {
    i64 create_comment(1:CreateCommentRequest request) throws (1:errors.ServiceException se)
    void delete_comment(1:DeleteCommentRequest request) throws (1:errors.ServiceException se)
    Comment get_user_comment(1:i64 user_id, 2:i64 comment_id) throws (1:errors.ServiceException se)
    Comment get_article_comment(1:i64 article_id, 2:i64 comment_id) throws (1:errors.ServiceException se)
    map<i64, Comment> get_comments_of_article(1:i64 article_id, 2:list<i64> comment_ids) throws (1:errors.ServiceException se)
    map<i64, Comment> get_comments_of_user(1:i64 user_id, 2:list<i64> comment_ids) throws (1:errors.ServiceException se)
    page.page_result get_user_comments(1:page.user_page request) throws (1:errors.ServiceException se)
    page.page_result get_user_articles(1:page.user_page request) throws (1:errors.ServiceException se)
    page.page_result get_article_comments(1:page.article_page request) throws (1:errors.ServiceException se)
    page.page_result get_article_like_comments(1:page.article_page request) throws (1:errors.ServiceException se)
    page.page_result get_article_latest_comments(1:page.article_page request) throws (1:errors.ServiceException se)
    i64 get_article_comment_floor(1:i64 article_id, 2:i64 comment_id) throws (1:errors.ServiceException se)
    void update_comment_floor(1:i64 comment_id, 2:i64 user_id, 3:i64 article_id, 4:i64 floor) throws (1:errors.ServiceException se)
    void update_comment_status(1:i64 comment_id, 2:i64 user_id, 3:i64 article_id, 4:string status) throws(1:errors.ServiceException se)
    void update_user_comments_status(1:i64 user_id, 2:i64 start_time, 3:i64 end_time 4:string status) throws(1:errors.ServiceException se)

    void create_comment_like(1:i64 user_id, 2:i64 article_id, 3:i64 comment_id) throws (1:errors.ServiceException se)
    void delete_comment_like(1:i64 user_id, 2:i64 article_id, 3:i64 comment_id) throws (1:errors.ServiceException se)

    list<i64> get_article_user_like_comments(1:i64 article_id, 2:i64 user_id, 3:list<i64> comment_ids) throws (1:errors.ServiceException se)
    page.page_result get_article_user_comments(1:page.article_user_page request) throws (1:errors.ServiceException se)
    list<Comment> get_user_comments_on_article(1:i64 user_id, 2:i64 article_id, 3:i32 offset, 4:i32 limit) throws (1:errors.ServiceException se)
    Comment get_comment_by_floor_on_article(1:i64 article_id, 2:i32 floor) throws (1:errors.ServiceException se)

    void forbid_user_comments(1:i64 user_id) throws(1:errors.ServiceException se)
}
