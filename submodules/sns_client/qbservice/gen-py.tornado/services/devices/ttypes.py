#
# Autogenerated by Thrift Compiler (0.9.3)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py:tornado
#

from thrift.Thrift import TType, TMessageType, TException, TApplicationException
import services.pages.ttypes
import services.errors.ttypes


from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol, TProtocol
try:
  from thrift.protocol import fastbinary
except:
  fastbinary = None



class Device:
  """
  Attributes:
   - id
   - user_id
   - app_version
   - enable_push
   - updated_at
   - token_type
   - push_token
   - device_id
   - exist
  """

  thrift_spec = (
    None, # 0
    (1, TType.I64, 'id', None, 0, ), # 1
    (2, TType.I64, 'user_id', None, None, ), # 2
    (3, TType.I64, 'app_version', None, None, ), # 3
    (4, TType.I64, 'enable_push', None, None, ), # 4
    (5, TType.I64, 'updated_at', None, None, ), # 5
    (6, TType.I64, 'token_type', None, None, ), # 6
    (7, TType.STRING, 'push_token', None, None, ), # 7
    (8, TType.STRING, 'device_id', None, None, ), # 8
    (9, TType.BOOL, 'exist', None, True, ), # 9
  )

  def __init__(self, id=thrift_spec[1][4], user_id=None, app_version=None, enable_push=None, updated_at=None, token_type=None, push_token=None, device_id=None, exist=thrift_spec[9][4],):
    self.id = id
    self.user_id = user_id
    self.app_version = app_version
    self.enable_push = enable_push
    self.updated_at = updated_at
    self.token_type = token_type
    self.push_token = push_token
    self.device_id = device_id
    self.exist = exist

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.I64:
          self.id = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.I64:
          self.user_id = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.I64:
          self.app_version = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.I64:
          self.enable_push = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 5:
        if ftype == TType.I64:
          self.updated_at = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 6:
        if ftype == TType.I64:
          self.token_type = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 7:
        if ftype == TType.STRING:
          self.push_token = iprot.readString()
        else:
          iprot.skip(ftype)
      elif fid == 8:
        if ftype == TType.STRING:
          self.device_id = iprot.readString()
        else:
          iprot.skip(ftype)
      elif fid == 9:
        if ftype == TType.BOOL:
          self.exist = iprot.readBool()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('Device')
    if self.id is not None:
      oprot.writeFieldBegin('id', TType.I64, 1)
      oprot.writeI64(self.id)
      oprot.writeFieldEnd()
    if self.user_id is not None:
      oprot.writeFieldBegin('user_id', TType.I64, 2)
      oprot.writeI64(self.user_id)
      oprot.writeFieldEnd()
    if self.app_version is not None:
      oprot.writeFieldBegin('app_version', TType.I64, 3)
      oprot.writeI64(self.app_version)
      oprot.writeFieldEnd()
    if self.enable_push is not None:
      oprot.writeFieldBegin('enable_push', TType.I64, 4)
      oprot.writeI64(self.enable_push)
      oprot.writeFieldEnd()
    if self.updated_at is not None:
      oprot.writeFieldBegin('updated_at', TType.I64, 5)
      oprot.writeI64(self.updated_at)
      oprot.writeFieldEnd()
    if self.token_type is not None:
      oprot.writeFieldBegin('token_type', TType.I64, 6)
      oprot.writeI64(self.token_type)
      oprot.writeFieldEnd()
    if self.push_token is not None:
      oprot.writeFieldBegin('push_token', TType.STRING, 7)
      oprot.writeString(self.push_token)
      oprot.writeFieldEnd()
    if self.device_id is not None:
      oprot.writeFieldBegin('device_id', TType.STRING, 8)
      oprot.writeString(self.device_id)
      oprot.writeFieldEnd()
    if self.exist is not None:
      oprot.writeFieldBegin('exist', TType.BOOL, 9)
      oprot.writeBool(self.exist)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    if self.user_id is None:
      raise TProtocol.TProtocolException(message='Required field user_id is unset!')
    if self.app_version is None:
      raise TProtocol.TProtocolException(message='Required field app_version is unset!')
    if self.enable_push is None:
      raise TProtocol.TProtocolException(message='Required field enable_push is unset!')
    if self.updated_at is None:
      raise TProtocol.TProtocolException(message='Required field updated_at is unset!')
    if self.token_type is None:
      raise TProtocol.TProtocolException(message='Required field token_type is unset!')
    if self.push_token is None:
      raise TProtocol.TProtocolException(message='Required field push_token is unset!')
    if self.device_id is None:
      raise TProtocol.TProtocolException(message='Required field device_id is unset!')
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.id)
    value = (value * 31) ^ hash(self.user_id)
    value = (value * 31) ^ hash(self.app_version)
    value = (value * 31) ^ hash(self.enable_push)
    value = (value * 31) ^ hash(self.updated_at)
    value = (value * 31) ^ hash(self.token_type)
    value = (value * 31) ^ hash(self.push_token)
    value = (value * 31) ^ hash(self.device_id)
    value = (value * 31) ^ hash(self.exist)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, value)
      for key, value in self.__dict__.iteritems()]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

  def __ne__(self, other):
    return not (self == other)

class AppVersion:
  """
  Attributes:
   - app_version
   - type
   - exist
  """

  thrift_spec = (
    None, # 0
    (1, TType.I64, 'app_version', None, None, ), # 1
    (2, TType.STRING, 'type', None, None, ), # 2
    (3, TType.BOOL, 'exist', None, True, ), # 3
  )

  def __init__(self, app_version=None, type=None, exist=thrift_spec[3][4],):
    self.app_version = app_version
    self.type = type
    self.exist = exist

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.I64:
          self.app_version = iprot.readI64()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.STRING:
          self.type = iprot.readString()
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.BOOL:
          self.exist = iprot.readBool()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('AppVersion')
    if self.app_version is not None:
      oprot.writeFieldBegin('app_version', TType.I64, 1)
      oprot.writeI64(self.app_version)
      oprot.writeFieldEnd()
    if self.type is not None:
      oprot.writeFieldBegin('type', TType.STRING, 2)
      oprot.writeString(self.type)
      oprot.writeFieldEnd()
    if self.exist is not None:
      oprot.writeFieldBegin('exist', TType.BOOL, 3)
      oprot.writeBool(self.exist)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    if self.app_version is None:
      raise TProtocol.TProtocolException(message='Required field app_version is unset!')
    if self.type is None:
      raise TProtocol.TProtocolException(message='Required field type is unset!')
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.app_version)
    value = (value * 31) ^ hash(self.type)
    value = (value * 31) ^ hash(self.exist)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, value)
      for key, value in self.__dict__.iteritems()]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

  def __ne__(self, other):
    return not (self == other)

class ExecResult:
  """
  Attributes:
   - success
   - affected
  """

  thrift_spec = (
    None, # 0
    (1, TType.BOOL, 'success', None, None, ), # 1
    (2, TType.I64, 'affected', None, None, ), # 2
  )

  def __init__(self, success=None, affected=None,):
    self.success = success
    self.affected = affected

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.BOOL:
          self.success = iprot.readBool()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.I64:
          self.affected = iprot.readI64()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('ExecResult')
    if self.success is not None:
      oprot.writeFieldBegin('success', TType.BOOL, 1)
      oprot.writeBool(self.success)
      oprot.writeFieldEnd()
    if self.affected is not None:
      oprot.writeFieldBegin('affected', TType.I64, 2)
      oprot.writeI64(self.affected)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    if self.success is None:
      raise TProtocol.TProtocolException(message='Required field success is unset!')
    if self.affected is None:
      raise TProtocol.TProtocolException(message='Required field affected is unset!')
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.success)
    value = (value * 31) ^ hash(self.affected)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, value)
      for key, value in self.__dict__.iteritems()]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

  def __ne__(self, other):
    return not (self == other)
