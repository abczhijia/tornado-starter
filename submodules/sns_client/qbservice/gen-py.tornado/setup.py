from setuptools import setup, find_packages

version = '0.0.1'

setup(
    name='qbservice',
    version=version,
    description="QiuBai users comments articles service",
    packages=find_packages(),
    #install_requires=['torndb'],
    platforms=['POSIX'],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX',
        'Programming Language :: Python',
    ],
    author="huronghua",
    author_email="huronghua@qiushibaike.com",
    url="git@git.moumentei.com:zeayes/qbservice.git",
)