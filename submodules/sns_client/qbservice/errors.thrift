namespace py services.errors
namespace go services.errors

enum ErrorCode {
    DATABASE_ERROR = 10001
    REDIS_ERROR = 10002
    MEMCACHE_ERROR = 10003
    SERIALIZATION_ERROR = 10004
    QUEYR_COUNT_OVERFLOW = 10005
    // user
    PASSWORD_WRONG = 20001
    USER_NOT_EXIST = 20002
    TOKEN_NOT_EXIST = 20003
    THIRD_ACCOUNT_NOT_EXIST = 20004
    AVATAR_HISTORY_NOT_EXIST = 20005
    EMAIL_BIND_NOT_EXIST = 20006
    VISIT_NOT_EXIST = 20007
    NICK_NOT_EXIST = 20008
    PUSH_BIND_NOT_EXIST = 20009
    PUNISH_NOT_EXIST = 20010

    // comment
    COMMENT_NOT_EXIST = 40001

    // article
    ARTICLE_NOT_EXIST = 50001
    ARTICLE_SERVER_ERROR = 50002

    INPUT_INVALID_ERROR = 90001
}

exception ServiceException {
    1: required ErrorCode code
    2: required string message
}
