
# Usage: sh thrift_gen.bash device

# thrift默认生成的go代码里使用的import导入路径是git.apache.org/thrift.git/lib/go/thrift(无法go get获取),
# 命令行指定thrift_import参数将其改为git-wip-us.apache.org/repos/asf/thrift.git/lib/go/thrift
thrift -gen go:thrift_import=git-wip-us.apache.org/repos/asf/thrift.git/lib/go/thrift "$1".thrift;

thrift -gen py "$1".thrift;
thrift -gen py:tornado "$1".thrift;
