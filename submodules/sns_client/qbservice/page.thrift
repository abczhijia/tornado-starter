namespace py services.pages
namespace go services.pages

struct user_page {
    1: required i64 user_id 
    2: optional i32 offset = 0
    3: optional i32 limit = 10
}

struct article_page {
    1: required i64 article_id
    2: optional i32 offset = 0
    3: optional i32 limit = 10
}

struct article_user_page{
    1: required i64 article_id
    2: required i64 user_id
    3: optional i32 offset = 0
    4: optional i32 limit = 10
}

struct page_result {
    1: required list<i64> ids
    2: required i32 total
}
