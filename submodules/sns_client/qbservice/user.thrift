include "errors.thrift"

namespace go services.users
namespace py services.users

struct CreateUserRequest {
    1: required i64 user_id
    2: required string login
    3: required string email
    4: required string state
    5: required string avatar
    6: required string salt
    7: required string password
    8: required string activation_code
    9: required string last_device
    10: required string old_uuid
    11: required string deviceid_info
    12: required string reg_source
    13: required string created_at
    14: required string updated_at
    15: required string last_visited_at
    16: required string avatar_updated_at
}

/*
state说明:
bonding: 老用户绑定邮箱未激活
binding: 新用户绑定邮箱未激活
pending: web邮箱注册未激活
active: app未绑定邮箱
bonded: 绑定邮箱已激活
suspended: 用户被封禁
*/
struct User {
    1: required i64 user_id
    2: required i32 bg_id
    3: required string job
    4: required string role
    5: required string login
    6: required string state
    7: required string email
    8: required string haunt
    9: required string hobby
    10: required string avatar
    11: required string gender
    12: required string emotion
    13: required string birthday
    14: required string password
    15: required string location
    16: required string hometown
    17: required string signature
    18: required string introduce
    19: required string big_cover
    20: required string last_device
    21: required string mobile_brand
    22: required string activation_code
    23: required string real_mobile_brand
    24: required i64 created_at
    25: required i64 updated_at
    26: required i64 last_visited_at
    27: required i64 avatar_updated_at
    28: required string big_cover_updated_at
    29: optional string mobile
}

struct Visit {
    1: required i64 id
    2: required i64 fid
    3: required i64 tid
    4: required string visited_at
}

struct VisitStat {
    1: required i64 user_id
    2: optional i64 duv = 0
    3: optional i64 tuv = 0
    4: optional i64 ltuv = 0
    5: optional string visited_at = ""
}

struct UserNick {
    1: required i64 user_id
    2: optional string nicks = ""
    3: optional i64 ver = 0
}

struct Token {
    1: required i64 user_id
    2: required string token
    3: required string expire_at
}

struct ThirdAccount {
    1: required i64 user_id
    2: required string account_type
    3: required string third_id
    4: required string name
    5: required string description
}

struct EmailBind {
   1: required i64 user_id
   2: required string email
   3: required string password
   4: required string created_at
}

struct ForgetPassword {
   1: required i64 user_id
   2: required string token
}

struct Forget{
   1: required i64 user_id
   2: required string token
   3: required string created_at
}

struct AvatarHistory {
    1: required i64 user_id
    2: required i32 times
    3: required i64 created_at
}

struct Punish {
    1: required i64 user_id
    2: required string option
    3: required string deadline
    4: required string reason
    5: required string created_at
}

service UserService {
    User create_user(1:CreateUserRequest user) throws(1:errors.ServiceException se)
    User get_user_by_id(1:i64 user_id) throws(1:errors.ServiceException se)
    User get_user_by_token(1:string token) throws(1:errors.ServiceException se)
    User get_user_by_login(1:string login) throws(1:errors.ServiceException se)
    User get_user_by_email(1:string email) throws(1:errors.ServiceException se)
    User get_user_by_mobile(1:string mobile) throws(1:errors.ServiceException se)
    map<i64, User> get_users_by_ids(1:list<i64> user_ids) throws(1:errors.ServiceException se)
    map<i64, User> search_users_by_login(1:list<string> user_ids, 2:string login, 3:i64 limit, 4:i64 offset) throws(1:errors.ServiceException se)
    void update_user_email(1:i64 user_id, 2:string email) throws(1:errors.ServiceException se)
    void update_user_login(1:i64 user_id, 2:string login) throws(1:errors.ServiceException se)
    void update_user_mobile(1:i64 user_id, 2:string mobile) throws(1:errors.ServiceException se)
    void set_user_avatar(1:i64 user_id, 2:string avatar, 3:string format) throws(1:errors.ServiceException se)
    void update_user_avatar(1:i64 user_id, 2:string avatar, 3:string format) throws(1:errors.ServiceException se)
    void reset_user_password(1:i64 user_id, 2:string password) throws(1:errors.ServiceException se)
    void update_user_password(1:i64 user_id, 2:string new_password, 3:string old_password) throws(1:errors.ServiceException se)
    void reset_pay_password(1:i64 user_id, 2:string password) throws(1:errors.ServiceException se)
    void update_pay_password(1:i64 user_id, 2:string new_password, 3:string old_password) throws(1:errors.ServiceException se)
    void update_user_status(1:i64 user_id, 2:string status) throws(1:errors.ServiceException se)
    void update_user_detail(1:i64 user_id, 2:map<string,string> user_detail) throws(1:errors.ServiceException se)
    void update_user_activation_code(1:i64 user_id 2:string code) throws(1:errors.ServiceException se)
    void activate_user_email(1:i64 user_id) throws(1:errors.ServiceException se)
    bool check_pay_password(1:i64 user_id, 2:string password) throws(1:errors.ServiceException se)
    User user_authenticate(1:string account, 2:string password) throws(1:errors.ServiceException se)
    AvatarHistory get_avatar_history(1:i64 user_id) throws(1:errors.ServiceException se)

    void create_visit(1:i64 tid, 2:i64 fid) throws(1:errors.ServiceException se)
    void delete_visit(1:i64 id) throws(1:errors.ServiceException se)
    void update_last_visit_time(1:i64 id) throws(1:errors.ServiceException se)
    Visit get_last_visit_by_tid(1:i64 tid) throws(1:errors.ServiceException se)
    Visit get_last_visit_by_fid(1:i64 tid, 2:i64 fid) throws(1:errors.ServiceException se)
    list<Visit> get_visits_by_time(1:string visited_at, 2:i64 limit) throws(1:errors.ServiceException se)

    void create_visit_stat(1:i64 user_id, 2:i64 duv, 3:i64 tuv, 4:i64 ltuv) throws(1:errors.ServiceException se)
    void init_visit_stat(1:i64 user_id) throws(1:errors.ServiceException se)
    void incr_visit_stat(1:i64 user_id) throws(1:errors.ServiceException se)
    void sync_visit_stat(1:i64 user_id, 2:bool clr_duv) throws(1:errors.ServiceException se)
    VisitStat get_last_visit_stat_by_id(1:i64 user_id) throws(1:errors.ServiceException se)
    VisitStat get_visit_count_by_time(1:i64 user_id, 2:string vistied_at) throws(1:errors.ServiceException se)

    /* 合并处理visit */
    void update_visit_stat(1:i64 user_id, 2:i64 visit_id) throws(1:errors.ServiceException se)
    VisitStat get_visit_stat(1:i64 user_id) throws(1:errors.ServiceException se)

    void create_user_nicks(1:i64 user_id, 2:string nicks) throws(1:errors.ServiceException se)
    void update_user_nicks(1:i64 user_id, 2:string nicks) throws(1:errors.ServiceException se)
    UserNick get_user_nicks_by_id(1:i64 user_id) throws(1:errors.ServiceException se)

    void create_email_bind(1:EmailBind bind) throws(1:errors.ServiceException se)
    void delete_email_bind(1:i64 user_id) throws(1:errors.ServiceException se)
    EmailBind get_email_bind(1:i64 user_id) throws(1:errors.ServiceException se)

    void create_forget_password(1:ForgetPassword forget) throws(1:errors.ServiceException se)
    /* 获取一条记录，若存在则更新其创建时间，若不存在则创建 */
    Forget get_user_forget(1:i64 user_id) throws(1:errors.ServiceException se)
    /* 通过token获取记录，验证token时候用到 */
    Forget get_forget_by_token(1:string token) throws(1:errors.ServiceException se)
    void delete_forget_by_token(1:string token)

    Token get_token_info(1:string token) throws(1:errors.ServiceException se)
    Token get_user_token(1:i64 user_id) throws(1:errors.ServiceException se)
    Token update_user_token(1:i64 user_id) throws(1:errors.ServiceException se)

    void create_third_account(1:ThirdAccount account) throws(1:errors.ServiceException se)
    ThirdAccount get_third_account(1:string account_type, 2:string third_id) throws(1:errors.ServiceException se)
    ThirdAccount get_user_third_account(1:i64 user_id, 2:string account_type) throws(1:errors.ServiceException se)
    list<ThirdAccount> get_third_accounts(1:i64 user_id) throws(1:errors.ServiceException se)
    map<string, ThirdAccount> get_type_accounts(1:string account_type, 2:list<string> third_ids) throws(1:errors.ServiceException se)
    void update_third_account(1:ThirdAccount account) throws(1:errors.ServiceException se)
    void delete_third_account(1:i64 user_id, 2:string account_type) throws(1:errors.ServiceException se)

    Punish get_punish_by_option(1:i64 user_id, 2:string option) throws(1:errors.ServiceException se)
    bool check_user_punish(1:i64 user_id, 2:string option, 3:i64 current_time) throws(1:errors.ServiceException se)
    bool check_uuid_black(1:string uuid) throws(1:errors.ServiceException se)
}
