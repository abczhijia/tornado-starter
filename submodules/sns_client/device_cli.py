# coding=utf-8

""" 获取某个版本号范围内的用户ID集 """

import time
import redis
import logging
import sys
logging.basicConfig(level=logging.INFO, stream=sys.stdout)


rdb = redis.StrictRedis(host='10.251.74.150', port=16381)
MAX_VALID_USER_ID = 35000000  # Note: 记得及时更新有效的最大用户ID


def _check_and_encode_version(min_version, max_version):
    assert isinstance(max_version, (int, long)) and 0 <= max_version <= 999999, "max_version必须为int类型, 且0<=x<=999999"
    assert isinstance(min_version, (int, long)) and 0 <= min_version <= 999999, "min_version必须为int类型, 且0<=x<=999999"
    assert min_version <= max_version, "min_version should <= max_version"
    return min_version << 4, max_version << 4


def _get_pairs_by_score_range(min_score, max_score, sleep_interval):
    pairs = []
    max_key_id = MAX_VALID_USER_ID / 128
    p = rdb.pipeline(transaction=False)
    for i in xrange(max_key_id):
        key = "ver_%s" % i
        p.zrangebyscore(key, min_score, max_score, withscores=True, score_cast_func=int)
        if (i % 100 == 0) or (i == max_key_id - 1):
            pipe_result = p.execute()
            for page in pipe_result:
                for one in page:
                    pairs.append((int(one[0]), one[1]))  # member(uid), score(version+device_type)
            time.sleep(sleep_interval)
    return pairs


def get_android_uids_by_version(min_version=0, max_version=999999, sleep_interval=0):
    """
      指定andorid版本号范围区间, 获取用户ID集

    - 仅指定min_version, 代表查找 >= min_version
    - 仅指定max_version, 代表查找 <= max_version
    - 都指定, 代表查找 [min_version, max_version]
    - 都不指定, 代表查找 任意版本

    min_version, max_version: 100601 --> 10.6.1
    """
    min_score, max_score = _check_and_encode_version(min_version, max_version)
    t1 = time.time()
    pairs = _get_pairs_by_score_range(min_score, max_score, sleep_interval)
    uids = [one[0] for one in pairs if one[1] & 0xF == 1]
    t2 = time.time()
    logging.info(u'android[%s,%s], user_count: %s, cost: %.1f second', min_version, max_version, len(uids), t2 - t1)
    return uids


def get_ios_uids_by_version(min_version=0, max_version=999999, sleep_interval=0):
    """ 指定iOS版本号范围区间, 获取用户ID集 """
    min_score, max_score = _check_and_encode_version(min_version, max_version)
    t1 = time.time()
    pairs = _get_pairs_by_score_range(min_score, max_score, sleep_interval)
    uids = [one[0] for one in pairs if one[1] & 0xF == 0]
    t2 = time.time()
    logging.info(u'ios[%s,%s], user_count: %s, cost: %.1f second', min_version, max_version, len(uids), t2 - t1)
    return uids


def get_all_uids_by_version(min_android_version=0, max_android_version=999999, min_ios_version=0, max_ios_version=999999, sleep_interval=0):
    """ 分别指定andorid和iOS版本号范围区间, 获取各自的用户ID集 """
    android_min_score, android_max_score = _check_and_encode_version(min_android_version, max_android_version)
    ios_min_score, ios_max_score = _check_and_encode_version(min_ios_version, max_ios_version)

    t1 = time.time()
    min_score, max_score = min(android_min_score, ios_min_score), max(android_max_score, ios_max_score)
    # 按两者的版本号范围的并集从redis取数据, 再在程序中各自筛选, 提高性能
    pairs = _get_pairs_by_score_range(min_score, max_score, sleep_interval)
    t2 = time.time()

    android_uids = [one[0] for one in pairs if ((one[1] & 0xF == 1) and (android_min_score <= one[1] <= android_max_score))]
    t3 = time.time()
    logging.info(u'android[%s,%s], user_count: %s, cost: %.1f second',
                 min_android_version, max_android_version, len(android_uids), t3 - t1)

    ios_uids = [one[0] for one in pairs if ((one[1] & 0xF == 0) and (ios_min_score <= one[1] <= ios_max_score))]
    t4 = time.time()
    logging.info(u'ios[%s,%s], user_count: %s, cost: %.1f second',
                 min_ios_version, max_ios_version, len(ios_uids), t2 - t1 + t4 - t3)
    return android_uids, ios_uids


if __name__ == '__main__':
    pass

    # android_uids1, ios_uids1 = get_all_uids_by_version(100600, 100900, 100500, 100900)

    # android_uids2 = get_android_uids_by_version(100600, 100900)
    # print "android: %s & %s" % (len(android_uids1), len(android_uids2)),
    # print set(android_uids1) - set(android_uids2), set(android_uids2) - set(android_uids1)
    # print "---------------------------------------------------------------"

    # ios_uids2 = get_ios_uids_by_version(100500, 100900)
    # print "ios: %s & %s" % (len(ios_uids1), len(ios_uids2)),
    # print set(ios_uids1) - set(ios_uids2), set(ios_uids2) - set(ios_uids1)
