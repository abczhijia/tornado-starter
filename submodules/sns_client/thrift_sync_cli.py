# -*- coding: utf-8 -*-

import os
import sys
import logging
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "qbservice/gen-py"))

from services.users import UserService, ttypes as user_ttypes
from services.comments import CommentService, ttypes as comment_ttypes
from services.articles import ArticleService, ttypes as article_ttypes
from services.devices import DeviceService, ttypes as device_ttypes
from services.pages import ttypes as page_ttypes
from services.errors import ttypes as error_ttypes

from thrift.protocol import TCompactProtocol, TBinaryProtocol
from thrift.transport import TSocket, TTransport

from __thrift_cli import Socket, make_user_info, make_user_detail, make_article_info, make_comment_info
from __thrift_cli import (
    DEFAULT_SERVICE_USER_HOST, DEFAULT_SERVICE_USER_PORT,
    DEFAULT_SERVICE_COMMENT_HOST, DEFAULT_SERVICE_COMMENT_PORT,
    DEFAULT_SERVICE_ARTICLE_HOST, DEFAULT_SERVICE_ARTICLE_PORT,
    DEFAULT_SERVICE_DEVICE_HOST, DEFAULT_SERVICE_DEVICE_PORT
)

# 非线程/协程安全, 不同线程/协程请各自使用其专用的AutoRetryClient()对象 或者 加锁
class AutoRetryClient(object):

    def __init__(self, client_creator, func_list=(), try_time=3, exc_handler=None):
        self.try_time = try_time
        self.client_creator = client_creator
        self.client, self._socket = None, None
        for func in func_list:
            if func[0] != '_':
                setattr(self, func, self.add_retry_func(func, exc_handler))
        self.create_client()

    def create_client(self):
        try:
            self.client, self._socket = self.client_creator()
        except:
            logging.warn(u'%s() error', self.client_creator.__name__)

    def ensure_client(self):
        if self._socket is None:
            self.create_client()

    def add_retry_func(self, func_name, exc_handler):
        def _(*args, **kwargs):
            try_time = self.try_time
            while try_time > 0:
                close_socket = False
                try:
                    self.ensure_client()
                    func = getattr(self.client, func_name)
                    result = func(*args, **kwargs)
                except Exception as e:
                    close_socket = True
                    if exc_handler is not None:
                        # 函数exc_handler负责根据函数名func_name和异常e, 决定是否需要关闭连接, 是否需要重试
                        # 返回值: 是否需要关闭连接, 是否需要重试, 不重试向上抛出啥异常, 或者不重试不抛异常直接返回啥
                        close_socket, need_retry, raise_exc, return_val = exc_handler(func_name, e)
                        if not need_retry:
                            if raise_exc is not None:
                                raise raise_exc
                            else:
                                return return_val
                    try_time -= 1
                    if try_time <= 0:
                        raise
                    logging.warn(u'%s(%s,%s) exception:%s,%s, retrying', func_name, args, kwargs, type(e), e)
                else:
                    return result
                finally:
                    if close_socket:
                        self.close_client()
        return _

    def close_client(self):
        if self._socket is not None:
            self._socket.close()
            self._socket = None

    # 调用一些耗时较长的RPC接口, 可先将超时调大, 调用完成后再调小, 避免每次都超时
    def set_timeout(self, ms):
        if self._socket:
            self._socket.setTimeout(ms)

    # 调整重试次数, 如: 调用create_xxx接口前将其设为1来临时禁用重试(避免重复创建)
    def set_try_time(self, try_time):
        self.try_time = try_time


def get_auto_retry_device_client(try_time=3, timeout=1000):
    def client_creator():
        device_socket = TSocket.TSocket(DEFAULT_SERVICE_DEVICE_HOST, DEFAULT_SERVICE_DEVICE_PORT)
        device_socket.open()
        device_socket.setTimeout(timeout)
        transport = TTransport.TFramedTransport(device_socket)
        protocol = TCompactProtocol.TCompactProtocol(transport)
        device_client = DeviceService.Client(protocol)
        return device_client, device_socket

    def exc_handler(func_name, e):
        if isinstance(e, error_ttypes.ServiceException):
            if e.code == error_ttypes.ErrorCode.INPUT_INVALID_ERROR:
                return False, False, e, None
        return True, True, None, None
    return AutoRetryClient(client_creator, func_list=dir(DeviceService.Iface()), exc_handler=exc_handler, try_time=try_time)


def get_auto_retry_user_client(try_time=3, timeout=1000):
    def client_creator():
        user_socket = TSocket.TSocket(DEFAULT_SERVICE_USER_HOST, DEFAULT_SERVICE_USER_PORT)
        user_socket.open()
        user_socket.setTimeout(timeout)
        transport = TTransport.TFramedTransport(user_socket)
        protocol = TCompactProtocol.TCompactProtocol(transport)
        user_client = UserService.Client(protocol)
        return user_client, user_socket

    known_exceptions = set([error_ttypes.ErrorCode.USER_NOT_EXIST, error_ttypes.ErrorCode.TOKEN_NOT_EXIST,
                            error_ttypes.ErrorCode.THIRD_ACCOUNT_NOT_EXIST, error_ttypes.ErrorCode.AVATAR_HISTORY_NOT_EXIST,
                            error_ttypes.ErrorCode.EMAIL_BIND_NOT_EXIST, error_ttypes.ErrorCode.VISIT_NOT_EXIST,
                            error_ttypes.ErrorCode.NICK_NOT_EXIST])

    def exc_handler(func_name, e):
        if isinstance(e, error_ttypes.ServiceException):
            if e.code in known_exceptions:
                return False, False, None, None
        return True, True, None, None
    return AutoRetryClient(client_creator, func_list=dir(UserService.Iface()), exc_handler=exc_handler, try_time=try_time)


def get_auto_retry_article_client(try_time=3, timeout=1000):
    def client_creator():
        article_socket = TSocket.TSocket(DEFAULT_SERVICE_ARTICLE_HOST, DEFAULT_SERVICE_ARTICLE_PORT)
        article_socket.open()
        article_socket.setTimeout(timeout)
        transport = TTransport.TFramedTransport(article_socket)
        protocol = TBinaryProtocol.TBinaryProtocol(transport)
        article_client = ArticleService.Client(protocol)
        return article_client, article_socket

    def exc_handler(func_name, e):
        if isinstance(e, error_ttypes.ServiceException):
            if e.code == error_ttypes.ErrorCode.ARTICLE_NOT_EXIST:
                return False, False, None, None
        return True, True, None, None
    return AutoRetryClient(client_creator, func_list=dir(ArticleService.Iface()), exc_handler=exc_handler, try_time=try_time)


def get_auto_retry_comment_client(try_time=3, timeout=1000):
    def client_creator():
        comment_socket = TSocket.TSocket(DEFAULT_SERVICE_COMMENT_HOST, DEFAULT_SERVICE_COMMENT_PORT)
        comment_socket.open()
        comment_socket.setTimeout(timeout)
        transport = TTransport.TFramedTransport(comment_socket)
        protocol = TCompactProtocol.TCompactProtocol(transport)
        comment_client = CommentService.Client(protocol)
        return comment_client, comment_socket

    def exc_handler(func_name, e):
        if isinstance(e, error_ttypes.ServiceException):
            if e.code == error_ttypes.ErrorCode.COMMENT_NOT_EXIST:
                return False, False, None, None
        return True, True, None, None
    return AutoRetryClient(client_creator, func_list=dir(CommentService.Iface()), exc_handler=exc_handler, try_time=try_time)


def example():
    # 创建一个全局的article_client对象(单线程独享使用, 或多线程加锁互斥使用), 后续直接使用此对象进行RPC调用(内部会进行异常重试, TCP连接重连)
    article_client = get_auto_retry_article_client()  # 可传入参数来调整默认的重试次数和超时时间
    print article_client.get_article_by_id(1)
    article_client.set_timeout(5000)  # 临时调整超时时间
    article_client.set_try_time(1)    # 临时禁用重试

    # 以下其他client和上面的article_client同理

    user_client = get_auto_retry_user_client()
    print user_client.get_user_by_id(1)
    print user_client.get_user_by_token('not_exist')

    device_client = get_auto_retry_device_client()
    print device_client.get_device_by_userid(1)

    comment_client = get_auto_retry_comment_client()
    print comment_client.get_user_comment(1, 1234)


if __name__ == '__main__':
    example()
