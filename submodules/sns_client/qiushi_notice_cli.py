#coding=utf-8

import time
import logging

from new_im_cli import str_msgid_generate, send_p2p_msg


# 糗事审核通过接口，article_data为一个文章的dict结构，见http://m2.qiushibaike.com/article/list/suggest接口返回
def ArticlePass(toid, article_data, desc, min_iv=None, min_av=None):
    logging.info("toid[%s] article passed:%s" % (toid, desc))
    return send_msg(toid, desc, article_data, "article", min_iv=min_iv, min_av=min_av)


# 糗事入选'1小时热门'通知
def ArticleHourHot(toid, article_data, desc, min_iv=None, min_av=None):
    logging.info("toid[%s] article hour hot:%s" % (toid, desc))
    return send_msg(toid, desc, article_data, "article", min_iv=min_iv, min_av=min_av)


def ArticleComment(toid, article_data, desc, min_iv=None, min_av=None):
    logging.info("toid[%s] article commented:%s" % (toid, desc))
    return send_msg(toid, desc, article_data, "article", min_iv=min_iv, min_av=min_av)


def ArticleUp(toid, article_data, desc, min_iv=None, min_av=None):
    logging.info("toid[%s] article praised:%s" % (toid, desc))
    return send_msg(toid, desc, article_data, "article", min_iv=min_iv, min_av=min_av)


def ArticleCommentAT(toid, article_data, desc, min_iv=None, min_av=None):
    logging.info("toid[%s] article commented at:%s" % (toid, desc))
    return send_msg(toid, desc, article_data, "comment", min_iv=min_iv, min_av=min_av)


def send_msg(toid, desc, jump_data, jump, notify=True, min_iv=None, min_av=None, min_replace_keys=None):
    fromid, fromnick, fromicon = 21089551, "糗事通知", "20150311173059.jpg"
    data = {
        "d": desc,
        "jump": jump,
        "jump_data": jump_data
    }
    if jump_data.get('m_type') in ('s_comment_like', 's_up'):  # 糗事评论单条点赞、糗事单条笑脸
        notify = False
    im_msg = {
        "type": 20,
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "to": toid,
        "msgid": str_msgid_generate(fromid, toid),
        "status": 1,
        "time": int(time.time() * 1000),
        "data": data,
        "usertype": 4,
        "notify": notify
    }
    if min_iv:
        im_msg['min_iv'] = min_iv
    if min_av:
        im_msg['min_av'] = min_av
    if min_replace_keys:
        im_msg['min_replace_keys'] = min_replace_keys
    send_p2p_msg(im_msg)
    logging.info(u'qiushi_notice, msg: %s', im_msg)
    return im_msg


if __name__ == '__main__':
    article = {
        "format": "image",
        "image": "112395097.jpg",
        "published_at": 1439805002,
        "tag": "",
        "user": {
            "avatar_updated_at": 1450795381,
            "uid": 20197905,
            "last_visited_at": 1409721393,
            "created_at": 1409721393,
            "state": "bonded",
            "last_device": "android_3.3.0",
            "role": "admin",
            "login": "soloph",
            "id": 20197905,
            "icon": "20151222144301.jpg"
        },
        "image_size": {
            "s": [220, 220, 1860],
            "m": [500, 501, 16532]
        },
        "id": 112395097,
        "votes": {
            "down": -43,
            "up": 85
        },
        "is_mine": False,
        "created_at": 1439797077,
        "content": "",
        "state": "publish",
        "comments_count": 11,
        "allow_comment": True,
        "share_count": 1,
        "type": "fresh"
    }
    article["from"] = {
        u'avatar_updated_at': 1450820105,
        u'last_visited_at': 1402865066,
        u'created_at': 1180231209,
        u'state': u'bonded',
        u'email': u'2simple@gmail.com',
        u'last_device': u'IMEI_d41d8cd98f00b204e9800998ecf8427e',
        u'role': u'admin',
        u'login': u'\u9ed1\u8863\u5927\u845b\u683c',
        u'id': u'1',
        u'icon': u'20151222213504.jpg'
    }
    article["jump_to_level"] = 3
    article["is_mine"] = True
    article["m_type"] = 's_comment'
    ArticleCommentAT(4065020, article, "我来评论一下", min_iv=110000)
    #ArticleCommentAT(23643645, article, "我来评论一下")
