# -*- coding: utf-8 -*-

import logging

from tornado.gen import coroutine, Return
from rpc_taf import Client, AsyncClient, retry, check_if_int

DEFAULT_CIRCLE_HOST_LIST = ["10.66.153.190:22001"]


class CircleClient(Client):
    def __init__(self, hosts=DEFAULT_CIRCLE_HOST_LIST, version=0x03):
        super(CircleClient, self).__init__(hosts, version)

    def get_my_medals(self, uid):
        return self._process_user_request('get_my_medals', {'uid': uid})

    def get_user_medals(self, uid):
        return self._process_user_request('get_user_medals', {'uid': uid})

    def delete_user_articles(self, uid):
        return self._process_user_request('delete_user_articles', {'uid': uid})

    def get_recent_article_info(self, uid):
        '''用户uid的最新一条动态'''
        return self._process_user_request('get_recent_article_info', {'uid': uid})

    def get_recent_articles_infos(self, uids):
        '''批量获取uids的最新一条动态集'''
        return self._process_users_request('get_recent_articles_infos', {'uids': uids})

    def get_user_score(self, uid):
        ''' 获取用户的积分和等级 '''
        return self._process_user_request('get_user_score', {'uid': uid})

    def get_user_scores(self, uids):
        ''' 批量获取用户的积分和等级 '''
        return self._process_users_request('get_user_scores', {'uids': uids})

    def increase_user_score(self, uid, change, change_type):
        ''' 获取用户的积分和等级 '''
        args = {'uid': uid, 'change': change, 'change_type': change_type}
        return self._process_user_request('increase_user_score', args)

    @retry
    def delete_article(self, article_id):
        '''删除帖子'''
        err, (article_id, ) = check_if_int(article_id)
        if err:
            logging.error('error input in circle_rpc_cli.delete_article()')
            return {'err': -1, "err_msg": "delete_article输入有误"}
        result = self.do_proc('delete_article', {'article_id': article_id})
        if not result:
            logging.error('error: function called error in circle_rpc_cli.delete_article()')
            return {'err': -1, "err_msg": "delete_article调用出错"}
        return result

    @retry
    def delete_comment(self, comment_id):
        '''删除评论'''
        err, (comment_id, ) = check_if_int(comment_id)
        if err:
            logging.error('error input in circle_rpc_cli.delete_comment()')
            return {'err': -1, "err_msg": "delete_comment输入有误"}
        kwargs = {
            "comment_id": comment_id
        }
        result = self.do_proc('delete_comment', kwargs)
        if not result:
            logging.error('error: function called error in circle_rpc_cli.delete_article()')
            return {'err': -1, "err_msg": "delete_article调用出错"}
        return result

    @retry
    def add_comment(self, article_id, who_am_i, content, comment_id=0):
        err, (article_id, who_am_i, comment_id) = check_if_int(article_id, who_am_i, comment_id)
        if err or (not isinstance(content, (str, unicode))) or len(content) > 512:
            logging.error('error input in circle_rpc_cli.add_comment()')
            return {'err': -1, "err_msg": "add_comment输入有误"}
        kwargs = {
            "article_id": article_id,
            "who_am_i": who_am_i,
            "comment_id": comment_id,
            "content": content
        }
        result = self.do_proc('add_comment', kwargs)
        if not result:
            logging.error('error: function called error in circle_rpc_cli.delete_article()')
            return {'err': -1, "err_msg": "delete_article调用出错"}
        return result


class AsyncCircleClient(AsyncClient):

    def __init__(self, server_list=DEFAULT_CIRCLE_HOST_LIST, socket_timeout=2):
        super(AsyncCircleClient, self).__init__(server_list, socket_timeout)

    @coroutine
    def get_my_medals(self, uid):
        resp = yield self._process_user_request('get_my_medals', {'uid': uid})
        raise Return(resp)

    @coroutine
    def get_user_medals(self, uid):
        resp = yield self._process_user_request('get_user_medals', {'uid': uid})
        raise Return(resp)

    @coroutine
    def delete_user_articles(self, uid):
        resp = yield self._process_user_request('delete_user_articles', {'uid': uid})
        raise Return(resp)

    @coroutine
    def get_recent_article_info(self, uid):
        resp = yield self._process_user_request('get_recent_article_info', {'uid': uid})
        raise Return(resp)

    @coroutine
    def get_recent_articles_infos(self, uids):
        '''批量获取uids的最新一条动态集'''
        resp = yield self._process_users_request('get_recent_articles_infos', {'uids': uids})
        raise Return(resp)

    @coroutine
    def get_user_score(self, uid):
        ''' 获取用户的积分和等级 '''
        resp = yield self._process_user_request('get_user_score', {'uid': uid})
        raise Return(resp)

    @coroutine
    def get_user_scores(self, uids):
        ''' 批量获取用户的积分和等级 '''
        resp = yield self._process_users_request('get_user_scores', {'uids': uids})
        raise Return(resp)

    @coroutine
    def increase_user_score(self, uid, change, change_type):
        ''' 获取用户的积分和等级 '''
        kwargs = {'uid': uid, 'change': change, 'change_type': change_type}
        resp = yield self._process_user_request('increase_user_score', kwargs)
        raise Return(resp)

    @coroutine
    def delete_article(self, article_id):
        '''删除帖子'''
        err, (article_id, ) = check_if_int(article_id)
        if err:
            logging.error('error input in circle_rpc_cli.delete_article()')
            raise Return({'err': -1, "err_msg": "delete_article输入有误"})
        result = yield self.do_proc('delete_article', {'article_id': article_id})
        if not result:
            logging.error('error: function called error in circle_rpc_cli.delete_article()')
            raise Return({'err': -1, "err_msg": "delete_article调用出错"})
        raise Return(result)

    @coroutine
    def delete_comment(self, comment_id):
        '''删除评论'''
        err, (comment_id, ) = check_if_int(comment_id)
        if err:
            logging.error('error input in circle_rpc_cli.delete_comment()')
            raise Return({'err': -1, "err_msg": "delete_comment输入有误"})
        kwargs = {
            "comment_id": comment_id
        }
        result = yield self.do_proc('delete_comment', kwargs)
        if not result:
            logging.error('error: function called error in circle_rpc_cli.delete_article()')
            raise Return({'err': -1, "err_msg": "delete_article调用出错"})
        raise Return(result)

    @coroutine
    def add_comment(self, article_id, who_am_i, content, comment_id=0):
        err, (article_id, who_am_i, comment_id) = check_if_int(article_id, who_am_i, comment_id)
        if err or (not isinstance(content, (str, unicode))) or len(content) > 512:
            logging.error('error input in circle_rpc_cli.add_comment()')
            raise Return({'err': -1, "err_msg": "add_comment输入有误"})
        kwargs = {
            "article_id": article_id,
            "who_am_i": who_am_i,
            "comment_id": comment_id,
            "content": content
        }
        result = yield self.do_proc('add_comment', kwargs)
        if not result:
            logging.error('error: function called error in circle_rpc_cli.delete_article()')
            raise Return({'err': -1, "err_msg": "delete_article调用出错"})
        raise Return(result)


if __name__ == "__main__":
    user_id = 27971380
    client = CircleClient()
    print client.get_my_medals(user_id)
    print client.get_user_medals(user_id)
    print client.get_user_score(user_id)
    import functools
    from tornado.ioloop import IOLoop
    client = AsyncCircleClient()
    IOLoop.current().run_sync(functools.partial(client.get_my_medals, user_id))
    IOLoop.current().run_sync(functools.partial(client.get_user_medals, user_id))
    IOLoop.current().run_sync(functools.partial(client.get_user_score, user_id))
    IOLoop.current().run_sync(functools.partial(client.get_recent_article_info, user_id))
