# -*- coding: utf-8 -*-

import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "qbservice/gen-py"))

import time
import logging
import inspect
import collections
from itertools import chain
from thrift.Thrift import TException
from thrift.transport.TSocket import TSocket
from thrift.transport.TTransport import TFramedTransport, TTransportException
from thrift.protocol.TBinaryProtocol import TBinaryProtocol
from thrift.protocol.TCompactProtocol import TCompactProtocol
from services.users import UserService
from services.comments import CommentService
from services.articles import ArticleService
from __thrift_cli import DEFAULT_SERVICE_USER_HOST, DEFAULT_SERVICE_USER_PORT
from __thrift_cli import DEFAULT_SERVICE_COMMENT_HOST, DEFAULT_SERVICE_COMMENT_PORT
from __thrift_cli import DEFAULT_SERVICE_ARTICLE_HOST, DEFAULT_SERVICE_ARTICLE_PORT


class Socket(TSocket):

    def __init__(self, host, port):
        TSocket.__init__(self, host, port)
        self.idle_at = time.time()


class ConnectionPool(object):
    ''' 连接池不限制活跃连接数，限制空闲连接的连接数和空闲时间 '''

    def __init__(self, client, protocol, host, port, socket_timeout=2, max_idle_connections=10, idle_timeout=5):
        self.host = host
        self.port = port
        self.client = client
        self.protocol = protocol
        self.idle_timeout = idle_timeout
        self.socket_timeout = socket_timeout
        self.idle_connections = collections.deque()
        self.active_connections = collections.deque()
        self.max_idle_connections = max_idle_connections

    def make_connection(self):
        socket = Socket(self.host, int(self.port))
        if self.socket_timeout > 0:
            socket.setTimeout(self.socket_timeout * 1000)
        transport = TFramedTransport(socket)
        transport.open()
        return self.client(self.protocol(transport))

    def get_connection(self):
        idle_count = len(self.idle_connections)
        # clean idle connection
        if self.idle_timeout > 0 and idle_count > self.max_idle_connections:
            now = time.time()
            while idle_count:
                idle_connection = self.idle_connections[idle_count - 1]
                if idle_connection.idle_at + self.idle_timeout > now:
                    self.close_connection(idle_connection)
                    self.idle_connections.remove(idle_connection)
                    logging.info('idle connection exceed: %s' % idle_connection)
                idle_count -= 1
        connection = self.idle_connections.popleft() if len(self.idle_connections) else self.make_connection()
        self.active_connections.append(connection)
        return connection

    def put_connection(self, connection):
        self.active_connections.remove(connection)
        if not connection._iprot.trans.isOpen():
            return
        if len(self.idle_connections) >= self.max_idle_connections:
            self.close_connection(connection)
            return
        connection.idle_at = time.time()
        return self.idle_connections.appendleft(connection)

    def close_connection(self, connection):
        connection._iprot.trans.close()

    def close(self):
        all_connections = chain(self.idle_connections, self.active_connections)
        for connection in all_connections:
            self.close_connection(connection)


class Client(object):

    def __init__(self, client, protocol, host, port, max_idle_connections=10, idle_timeout=5):
        self.pool = ConnectionPool(client, protocol, host, port, max_idle_connections, idle_timeout)
        for m in inspect.getmembers(client, predicate=inspect.ismethod):
            setattr(self, m[0], self.__proxy__(m[0]))

    def close(self):
        self._pool.close()

    def __proxy__(self, method):
        def decorator(*args, **kwargs):
            connection = self.pool.get_connection()
            try:
                response = getattr(connection, method)(*args, **kwargs)
            except TException as e:
                raise e
            except TTransportException as e:
                self.pool.close_connection(connection)
                raise e
            except Exception, e:
                self.pool.close_connection(connection)
                raise e
            finally:
                self.pool.put_connection(connection)
            return response
        return decorator


user_client = Client(UserService.Client, TCompactProtocol,
                     DEFAULT_SERVICE_USER_HOST, DEFAULT_SERVICE_USER_PORT)
comment_client = Client(CommentService.Client, TCompactProtocol,
                        DEFAULT_SERVICE_COMMENT_HOST, DEFAULT_SERVICE_COMMENT_PORT)
article_client = Client(ArticleService.Client, TBinaryProtocol,
                        DEFAULT_SERVICE_ARTICLE_HOST, DEFAULT_SERVICE_ARTICLE_PORT)
