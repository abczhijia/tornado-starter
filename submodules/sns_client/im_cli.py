#coding=utf-8

import time
import random
import json
import redis
import hashlib
import logging

from new_im_cli import send_p2p_msg, send_tribe_msg


g_md5 = hashlib.md5()
g_mq_redis = redis.StrictRedis(host='10.251.74.150', port=16379)
g_gmq_redis = redis.StrictRedis(host='10.250.156.31', port=16379)


def im_system_msg(fromid, toid, content, t, fromnick, fromicon, fromgender='', fromage=0, ut=0, system=0, notify=True, trint='', msgid='', push=True):
    if trint:
        _t, _rint = trint.split("_", 2)  # 群通知需要用确定的_t, _rint
        _t, _rint = int(_t), int(_rint)
    else:
        _t, _rint = int(time.time() * 1000), random.randint(2, 10000)

    if not msgid:
        g_md5.update("%s_%s_%s"%(_t, fromid, toid))
        msgid = g_md5.hexdigest()[:16]

    msg = {}
    msg["type"] = t
    msg["phone_ver"] = "4.0.0"
    msg["from"] = fromid
    msg["fromnick"] = fromnick
    msg["fromicon"] = fromicon
    if fromgender:
        msg["fromgender"] = fromgender
    if fromage:
        msg["fromage"] = fromage
    msg["to"] = toid
    msg["msgid"] = msgid
    msg["status"] = 1
    msg["time"] = _t
    msg["data"] = content #dict或者str，"安全提示：如果聊天内容包含钱等敏感信息，请先核实对方身份"
    if ut:
        msg["usertype"] = ut
    if system:
        msg["system"] = 1
    if not notify:
        msg["notify"] = False
    if push:
        send_p2p_msg(msg)
    return json.dumps(msg)


'''
根据tribeid获取群成员，并分发给所有群成员
'''
def im_tribe_msg(tribeid, tribename, tribeicon, fromid, t, ex_create, fromnick, fromicon, fromgender, fromage, content):
    if isinstance(content, dict):
        content = json.dumps(content)
    msg = {}
    msg["type"] = t  # 300:正常消息，301:系统消息（1.群大换届、2.组员被踢、3.组员/全体被禁言、4.组员/全体被解禁），302:新人进群
    msg["phone_ver"] = "4.0.0"
    msg["phone_type"] = ""
    msg["gid"] = tribeid
    msg["gnick"] = tribename
    if tribeicon:
        msg["gicon"] = tribeicon
    msg["from"] = fromid
    msg["fromnick"] = fromnick
    msg["fromicon"] = fromicon
    msg["fromgender"] = fromgender
    msg["fromage"] = fromage
    msg["to"] = 0
    msg["msgid"] = hashlib.md5("%s_%s_%s" %(fromid, tribeid, time.time())).hexdigest()[:12]  # hash(from_tid_时间戳)，区分客户端消息，这个取12位
    msg["status"] = 1
    msg["time"] = int(time.time() * 1000)
    msg["data"] = content  # "安全提示：如果聊天内容包含钱等敏感信息，请先核实对方身份"
    msg["system"] = 1
    msg["ex_create"] = ex_create  # 是否创建消息
    send_tribe_msg(msg)
    logging.info("tribe_msg, msg: %s", msg)
    return json.dumps(msg)


def test_im_tribe_msg():
    im_tribe_msg(30664, 'fortest', "", 0, 301, 0, "", "", "", 0, {"data": "a 被解除禁言", "sid": 23643645, "type": 4})


if __name__ == '__main__':
    pass
