# -*- coding: utf-8 -*-

import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "qbservice/gen-py.tornado"))

from services.users import UserService, ttypes as user_ttypes
from services.comments import CommentService, ttypes as comment_ttypes
from services.articles import ArticleService, ttypes as article_ttypes
from services.devices import DeviceService, ttypes as device_ttypes
from services.pages import ttypes as page_ttypes
from services.errors import ttypes as error_ttypes

from thrift.protocol import TCompactProtocol, TBinaryProtocol

from __thrift_cli import TClient, make_user_info, make_user_detail, make_article_info, make_comment_info
from __thrift_cli import (
    DEFAULT_SERVICE_USER_HOST, DEFAULT_SERVICE_USER_PORT,
    DEFAULT_SERVICE_COMMENT_HOST, DEFAULT_SERVICE_COMMENT_PORT,
    DEFAULT_SERVICE_ARTICLE_HOST, DEFAULT_SERVICE_ARTICLE_PORT,
    DEFAULT_SERVICE_DEVICE_HOST, DEFAULT_SERVICE_DEVICE_PORT
)


def get_async_user_client(host=DEFAULT_SERVICE_USER_HOST, port=DEFAULT_SERVICE_USER_PORT):
    return TClient(host, port, UserService, TCompactProtocol.TCompactProtocolFactory())


def get_async_comment_client(host=DEFAULT_SERVICE_COMMENT_HOST, port=DEFAULT_SERVICE_COMMENT_PORT):
    return TClient(host, port, CommentService, TCompactProtocol.TCompactProtocolFactory())


def get_async_article_client(host=DEFAULT_SERVICE_ARTICLE_HOST, port=DEFAULT_SERVICE_ARTICLE_PORT):
    return TClient(host, port, ArticleService, TBinaryProtocol.TBinaryProtocolFactory())


def get_async_device_client(host=DEFAULT_SERVICE_DEVICE_HOST, port=DEFAULT_SERVICE_DEVICE_PORT):
    return TClient(host, port, DeviceService, TCompactProtocol.TCompactProtocolFactory())


def example():
    import tornado.ioloop
    from tornado.gen import coroutine, Return

    @coroutine
    def _():
        client = get_async_device_client()
        rs = yield client.get_device_by_userid(4065020)
        print rs
        raise Return(rs)

    tornado.ioloop.IOLoop.instance().run_sync(_)


if __name__ == '__main__':
    example()
