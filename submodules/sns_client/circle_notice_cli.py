# coding=utf-8

import time
import logging

from new_im_cli import str_msgid_generate, send_p2p_msg

'''
-赞
B赞A的帖子
A收到消息（B ≧▽≦ 赞了你）

-评论（包括投票）
B在X楼评论A的帖子
A收到消息（B：内容）

-回复（评论中@）
C在A的帖子X楼下回复B
A收到消息（C 回复 B：内容）
B收到消息（C：内容）

-动态中@
「@了你」

赞的通知跳转到单贴详情页
评论、回复的通知跳转到相应的楼层
'''


def ArticleLike(toid, article_data, desc):
    logging.info("toid[%s] article liked:%s" % (toid, desc))
    article_data["m_type"] = 101
    return send_msg(toid, desc, article_data, "like", notify=False)


# need_union: 客户端需要聚合
def ArticleComment(toid, article_data, desc, need_union=False):
    logging.info("toid[%s] article commented:%s" % (toid, desc))
    article_data["m_type"] = 102 if need_union else 2
    return send_msg(toid, desc, article_data, "comment")


# comment_at: 客户端会在通知信息中拼凑: 回复 article_data['re_name']:
def ArticleCommentAT(toid, article_data, desc, need_union=False):
    logging.info("toid[%s] article commented at:%s" % (toid, desc))
    article_data["m_type"] = 103 if need_union else 3
    return send_msg(toid, desc, article_data, "comment_at")


def ArticleAT(toid, article_data, desc):
    logging.info("toid[%s] article at:%s" % (toid, desc))
    article_data["m_type"] = 4
    return send_msg(toid, desc, article_data, "comment")


def CommentLike(toid, article_data, desc):
    logging.info("toid[%s] comment liked:%s" % (toid, desc))
    article_data["m_type"] = 105
    return send_msg(toid, desc, article_data, 'like', notify=False)


# 动态被转发, 通知所转发的直接动态的所有人toid
def ArticleForwarded(toid, article_data, desc):
    logging.info("toid[%s] article forwarded:%s" % (toid, desc))
    article_data["m_type"] = 106
    return send_msg(toid, desc, article_data, "forwarded", min_iv='10.5.0', min_av='10.5.0')


def send_msg(toid, desc, jump_data, jump, notify=True, min_iv=None, min_av=None, min_replace_keys=None):
    fromid, fromnick, fromicon = 27685144, "糗友圈通知", "20150427163228.jpg"
    data = {
        "d": desc,
        "jump": jump,
        "jump_data": jump_data
    }
    im_msg = {
        "type": 20,
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "to": toid,
        "msgid": str_msgid_generate(fromid, toid),
        "status": 1,
        "time": int(time.time() * 1000),
        "data": data,
        "usertype": 4,
        "notify": notify
    }
    if min_av:
        im_msg['min_iv'] = min_iv
    if min_av:
        im_msg['min_av'] = min_av
    if min_replace_keys:
        im_msg['min_replace_keys'] = min_replace_keys
    send_p2p_msg(im_msg)
    logging.info(u"circe_notice, msg: %s", im_msg)
    return im_msg


if __name__ == '__main__':
    article_data = {
        "id": 636744,
        "status": 1,
        "content": u"\u8fd8\u6709\u4eba\u7fa4\u6d88\u606f\u5ef6\u8fdf\u5417\uff1f",
        "location": u"\u6df1\u5733\u5e02\u00b7\u5357\u5c71\u533a",
        "type": 2,
        "topic_id": 0,
        "created_at": 1445952651,
        "pic_urls": [],
        "from": {
            u'avatar_updated_at': 1450820105,
            u'last_visited_at': 1402865066,
            u'created_at': 1180231209,
            u'state': u'bonded',
            u'email': u'2simple@gmail.com',
            u'last_device': u'IMEI_d41d8cd98f00b204e9800998ecf8427e',
            u'role': u'admin',
            u'login': u'\u9ed1\u8863\u5927\u845b\u683c',
            u'id': u'1',
            u'icon': u'20151222213504.jpg'
        },
    }
    toids = [4065020, 23643645]
    for toid in toids:
        ArticleLike(toid, article_data, "赞了你")
        article_data["comment_id"] = 4283803
        ArticleComment(toid, article_data, "哈哈哈哈哈哈")
        article_data['re_name'] = "arch22"
        ArticleCommentAT(toid, article_data, "你好你好你好")
        ArticleForwarded(toid, article_data, "动态转发")
