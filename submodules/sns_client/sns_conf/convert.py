# coding: utf-8

import re
import time
import datetime
import logging
from job_list import JOB_VALUES, ADJUST_JOBS
from local_list import CITY_VALUES, COUNTY_VALUES, ADJUST_CITYS, ADJUST_COUNTY
from mobile_brand_list import BRANDS, MAGIC_BRANDS

'''compatibility. from tornado.escape import utf8'''

if type('') is not type(b''):
    def u(s):
        return s
    bytes_type = bytes
    unicode_type = str
    basestring_type = str
else:
    def u(s):
        return s.decode('unicode_escape')
    bytes_type = str
    unicode_type = unicode
    basestring_type = basestring

_UTF8_TYPES = (bytes_type, type(None))

def utf8(value):
    if isinstance(value, _UTF8_TYPES):
        return value
    if not isinstance(value, unicode_type):
        raise TypeError(
            "Expected bytes, unicode, or None; got %r" % type(value)
        )
    return value.encode("utf-8")

def convert_birthday_to_age(birthday):
    birthday = utf8(birthday)
    birth = [int(x) for x in birthday.split(" ")[0].split("-")]
    now = datetime.datetime.now()
    if now.year < birth[0] and now.month < birth[1] and now.day < birth[2]:
        return 0
    age = now.year - birth[0]
    if age != 0 and (now.month < birth[1] or (now.month == birth[1] and now.day < birth[2])):
        age -= 1
    return age

def convert_birthday_to_astrology(birthday):
    """根据生日计算星座，生日为yyyy-mm-dd格式的字符串"""
    birthday = utf8(birthday)
    birthday = birthday.split(" ")[0].split('-')
    month = int(birthday[1])
    day = int(birthday[2])
     
    zodiac_map = {
        '白羊座': [(3,21), (4,19)],
        '金牛座': [(4,20), (5,20)],
        '双子座': [(5,21), (6,21)],
        '巨蟹座': [(6,22), (7,22)],
        '狮子座': [(7,23), (8,22)],
        '处女座': [(8,23), (9,22)],
        '天秤座': [(9,23), (10,23)],
        '天蝎座': [(10,24), (11,22)],
        '射手座': [(11,23), (12,21)],
        '水瓶座': [(1,21), (2,18)],
        '双鱼座': [(2,19), (3,20)]
    }

    for key, value in zodiac_map.iteritems():
        if value[0] <= (month, day) <= value[1]:
            return key

    if (month, day) >= (12, 22) or (month, day) <= (1,20):
        return '摩羯座'

def convert_big_cover(big_cover):
    if not big_cover:
        return ""
    big_cover = utf8(big_cover)
    if big_cover.startswith("http://") and big_cover.endswith("/q/80"):
        return big_cover
    elif big_cover.startswith("http://"):
        return big_cover + '?imageView2/2/w/500/q/80'
    else:
        return 'http://cover.qiushibaike.com/' + big_cover + '?imageView2/2/w/500/q/80'

def convert_tribe_pic_url(pic_url):#pic_name:2待审/1通过/0不通过/1234567890不通过nopic,jpg
    if not pic_url:
        return "", -1
    pic_url = utf8(pic_url)
    if pic_url.startswith("http://") and pic_url.endswith("/q/80"):
        return pic_url, 1
    elif pic_url.startswith("http://"):
        return pic_url + '?imageView2/2/w/500/q/80', 1
    else:
        try:
            pic_url, st = pic_url.split(":")
        except Exception, e:
            logging.warn("split tribe pic_url error: %s, pic_url: %s" % (e, pic_url))
            st = 2
        return 'http://tribe-pic.qiushibaike.com/' + pic_url + '?imageView2/2/w/500/q/80', int(st)

def get_pic_eday(pic_url, last_updated_at):
    if not last_updated_at:
        return 0
    interval = last_updated_at and int(time.time() - last_updated_at) or 0
    if "nopic.jpg" in utf8(pic_url) and interval < 86400 * 90:#如果审核未通过的，限制期为90天
        pic_eday = 90 - interval / 86400
    elif interval < 3600 or interval > 86400 * 7:
        pic_eday = 0
    else:
        pic_eday = 7 - interval / 86400
    return pic_eday

def convert_mobile_brand(mobile_brand):
    if not mobile_brand:
        return ""
    mobile_brand = utf8(mobile_brand).lower()
    if mobile_brand.count(":"):
        mobile_brand, idx = mobile_brand.split(":")
        if MAGIC_BRANDS.has_key(mobile_brand):
            if len(MAGIC_BRANDS[mobile_brand]) > int(idx):
                return MAGIC_BRANDS[mobile_brand][int(idx)]
            else:
                return "Fight Club"
        elif int(idx) > 0:
            return "Fight Club"
    if BRANDS.has_key(mobile_brand):
        return BRANDS[mobile_brand]
    else:
        # logging.info('%s is not found in the list' % (mobile_brand))
        if re.search('lenovo', mobile_brand):
            return '联想'
        elif re.search('shv-|sm-|samsung', mobile_brand):
            return '三星'
        elif re.search('htc', mobile_brand):
            return 'HTC'
        elif re.search('zte', mobile_brand):
            return '中兴'
        elif re.search('tcl', mobile_brand):
            return 'TCL'
        elif re.search('coolpad', mobile_brand):
            return '酷派'
        elif re.search('k-touch', mobile_brand):
            return '天语'
        elif re.search('gn', mobile_brand):
            return '金立'
        elif re.search('vivo', mobile_brand):
            return 'Vivo'
        elif re.search('uoogou', mobile_brand):
            return '优购'
        elif re.search('t-smart', mobile_brand):
            return '天迈'
        elif re.search('tooky', mobile_brand):
            return 'Tooky京琦'
        elif re.search('meitu', mobile_brand):
            return '美图'
        else:
            if re.search('iphone|ipad|ipod ', mobile_brand):
                return 'iOS'
            return 'Android'

def is_processed_mobile_brand(mobile_brand):
    if not mobile_brand:
        return False
    mobile_brand = utf8(mobile_brand)
    for k, v in BRANDS.items():
        if mobile_brand == v:
            return k
    return False

def convert_haunt(haunt):
    if not haunt:
        return ""
    haunt = utf8(haunt).lower()
    if haunt == "hide":
        return ""
    city, _, county = haunt.partition("·")
    city = city.strip()
    county = county.strip()
    if ADJUST_CITYS.has_key(city.lower()):
        city = ADJUST_CITYS.get(city.lower())
    else:
        filter_pattern = "市$|市市辖区$|市辖区$|半岛$|岛$|地区$|.*路$"
        key_word = re.findall(filter_pattern, city)
        for word in key_word:
            city = city.replace(word, "")
    if ADJUST_COUNTY.has_key(county.lower()):
        county = ADJUST_COUNTY.get(county.lower())
    else:
        filter_pattern = "市$|新区$|市市辖区$|市辖区$|区$|县$|.*路$"
        key_word = re.findall(filter_pattern, county)
        for word in key_word:
            county = county.replace(word, "")
    return city + " · " + county if (city and county) else (city or county)

def convert_hometown(hometown):
    if not hometown:
        return ""
    hometown = utf8(hometown).lower()
    province, _, city = hometown.partition("·")
    province = province.strip()
    city = city.strip()
    if province not in COUNTY_VALUES or city not in CITY_VALUES:
        return "未知"
    if province==city:
        return city
    return province + " · " + city if (province and city) else (province or city)

def convert_job(job):
    if not job:
        return ""
    job = utf8(job)
    if job not in JOB_VALUES:
        return "未知"
    return ADJUST_JOBS.get(job, job)

def get_video_url(filename, video_type):
    video_url = "http://qiubai-video.qiushibaike.com/"
    if video_type == 1:
        video_url += filename
    elif video_type == 2:
        video_url += filename[0:-4] + "_3g.mp4"
    else:
        video_url += filename[0:-4] + ".jpg"
    return video_url


def handle_article_image(item):
    if not item['image']:
        return
    if item['image'][-4:] == '.mp4':
        item['high_url'] = get_video_url(item['image'], 1)
        item['low_url'] = get_video_url(item['image'], 2)
        item['pic_url'] = get_video_url(item['image'], 3)
        item['pic_size'] = [480, 480]
        # item['loop'] = fetch_video_loop(item['id'])
        item['image'] = None
    else:
        item['image'] = item['image'].split('.')[0] + ".jpg"
