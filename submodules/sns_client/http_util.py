# --*-- coding:utf8 --*--
import numbers
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.gen import coroutine, Return

# CurlAsyncHTTPClient is faster, but default SimpleAsyncHTTPClient has waiting queue for the request.
# AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")


_HTTPClient = AsyncHTTPClient(max_clients=20, defaults={"request_timeout": 3, "connect_timeout": 3})


@coroutine
def fetch(request, callback=None, raise_error=True, **kwargs):
    request_timeout = kwargs.pop("request_timeout", None)
    if request_timeout:
        assert isinstance(request_timeout, numbers.Real) and request_timeout > 0, "request_timeout should be a positive real"
        if isinstance(request, HTTPRequest):
            request.request_timeout = request_timeout
        else:
            kwargs["request_timeout"] = request_timeout
    response = yield _HTTPClient.fetch(request, callback=None, raise_error=True, **kwargs)
    raise Return(response)

if __name__ == "__main__":
    from tornado.ioloop import IOLoop
    from functools import partial
    IOLoop.instance().run_sync(partial(fetch, "http://m2.qiushibaike.com/article/list/hot"))
    IOLoop.instance().run_sync(partial(fetch, "http://m2.qiushibaike.com/article/list/hot", request_timeout=0.1))
    req = HTTPRequest("http://m2.qiushibaike.com/article/list/hot")
    IOLoop.instance().run_sync(partial(fetch, req, request_timeout=0.1))