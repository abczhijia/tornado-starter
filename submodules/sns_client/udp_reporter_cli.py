# -*- coding: utf-8

import socket
import json
import threading

AGG_CNT = 10

acc_mutex = threading.Lock()
set_mutex = threading.Lock()

SOCK = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
SOCK.setblocking(False)
ADDR_ACC = ('t-zabbix-report', 6668)
ADDR_SET = ('t-zabbix-report', 6669)

acc_cnt = 0
acc_item = dict()
def report_accumulator(key, val, s=False):
    global acc_cnt
    global acc_item

    try:
        acc_mutex.acquire()
        if key in acc_item:
            acc_item[key] += val
        else:
            acc_item[key] = val
        acc_cnt += 1
        if acc_cnt >= AGG_CNT or s:
            item = json.JSONEncoder().encode(acc_item)
            SOCK.sendto(item, ADDR_ACC)
            acc_item.clear()
            acc_cnt = 0
    finally:
        acc_mutex.release()


set_cnt = 0
set_item = dict()
def report_set(set_name, uuid, s=False):
    if not uuid:
        return

    global set_cnt
    global set_item

    try:
        set_mutex.acquire()
        if set_name in set_item:
            set_item[set_name].append(uuid)
        else:
            set_item[set_name] = [uuid]
        set_cnt += 1
        if set_cnt >= AGG_CNT or s:
            item = json.JSONEncoder().encode(set_item)
            SOCK.sendto(item, ADDR_SET)
            set_item.clear()
            set_cnt = 0
    finally:
        set_mutex.release()
