#coding=utf-8

import time
import json
import logging

from new_im_cli import str_msgid_generate, _create_mysql_push_msg, send_p2p_msg


# 生成 "直播开始通知" 实体(MySQL)消息
def create_live_start_immsg(fromid, fromnick, fromicon, dumped_data):
    # fromid开始直播, 通知toid, type=26(直播开始小纸条推送)
    # data={live_id, pic_url, title, content, origin, host_id, host_icon}
    _fromid, _fromnick, _fromicon = 32862889, '直播', '2016110316074457.JPEG'
    now = int(time.time() * 1000)
    im_msg = {
        "type": 26,  # 直播开始通知 - 消息类型
        "phone_ver": "4.0.0",
        "from": fromid,
        # "to": toid,   # NOTE: 发送给用户前, 再针对具体哪个用户, 加上此to字段
        "fromnick": fromnick,
        "fromicon": fromicon,
        "msgid": str_msgid_generate(fromid, 0),
        "status": 1,
        "time": now,
        "data": dumped_data,
        "notify": True,
        "seq_type": 'push',
        'max_iv': '10.6.0',
        'max_av': '10.5.0',
        'max_replace_keys': {
            'from': _fromid,
            'fromnick': _fromnick,
            'fromicon': _fromicon,
            'usertype': 1
        }
    }
    push_id = _create_mysql_push_msg(im_msg)
    return push_id, im_msg


# 家族通知消息
def FamilyNotice(toid, cnt):
    fromid, fromnick, fromicon = 33747598, '家族消息', '20170509142654.JPEG'
    im_msg = {
        "type": 1,
        "phone_ver": "4.0.0",
        "from": fromid,
        "to": toid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "msgid": str_msgid_generate(fromid, 0),
        "status": 1,
        "time": int(time.time() * 1000),
        "data": cnt,
    }
    send_p2p_msg(im_msg)
    logging.info(u'family_notice, msg: %s', im_msg)
    return im_msg


if __name__ == '__main__':
    pass
    # from new_im_cli import batch_send_msg
    # live_title = u'这个是直播标题'
    # fromid, fromnick, fromicon = 31853821, u'dede-de-干巴爹-杰佬', '20161013203517.jpg'
    # data = {
    #     'title': live_title,
    #     'origin': 2,
    #     'live_id': 69250581706850003,
    #     'host_id': fromid,
    #     'host_icon': fromicon,
    #     'pic_url': 'http://avatar.huoshantv.com/O3IZ8QERL5BE58KB.jpg?imageMogr2/format/jpg/thumbnail/300x300/auto-orient',
    #     'content': u'%s 开始新的直播啦，快来围观~「%s」' % (fromnick, live_title),
    # }
    # dumped_data = json.dumps(data)
    # push_id, im_msg = create_live_start_immsg(fromid, fromnick, fromicon, dumped_data)
    # batch_send_msg(push_id, im_msg, [4065020, 23643645])
    # FamilyNotice(4065020, '奥美巴扬 申请加入家族 外围女主播')
