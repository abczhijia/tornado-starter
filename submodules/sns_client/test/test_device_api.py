#coding=utf-8

import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../qbservice/gen-py"))
sys.path.append(".")
sys.path.append("..")
from thrift_sync_cli import get_auto_retry_device_client
from services.devices import ttypes as device_ttypes


device_client = get_auto_retry_device_client()


def assert_equal(d1, d2):
    assert d1.exist == d2.exist
    assert d1.user_id == d2.user_id
    assert d1.token_type == d2.token_type
    assert d1.app_version == d2.app_version
    assert d1.push_token == d2.push_token
    assert d1.updated_at == d2.updated_at
    assert d1.enable_push == d2.enable_push
    assert d1.device_id == d2.device_id


# 检查服务端数据是否已经更新
def assert_device_is_updated(local_device):
    d1 = device_client.get_device_by_deviceid(local_device.device_id)
    print d1
    assert_equal(d1, local_device)

    if local_device.user_id > 0:
        d2 = device_client.get_device_by_userid(local_device.user_id)
        print d2
        assert_equal(d2, local_device)


# 检查服务端数据是否已经删除
def assert_device_is_not_exist(local_device):
    print local_device

    d1 = device_client.get_device_by_deviceid(local_device.device_id)
    print d1
    assert d1.exist is False

    d2 = device_client.get_device_by_userid(local_device.user_id)
    print d2
    assert d2.exist is False


def test():
    # fake data
    null_user_id = -100
    not_exist_uid = 47654321
    token_type, app_version = 1, 100301
    push_token = "test_push_token"
    device_id = "test_device_id"
    updated_at = 1234
    enable_push = 1
    fake_device = device_ttypes.Device(user_id=not_exist_uid, token_type=token_type, app_version=app_version, push_token=push_token,
                                       device_id=device_id, updated_at=updated_at, enable_push=enable_push)

    # 1. 确保不存在
    _ = device_client.delete_device_by_userid(fake_device.user_id)
    _ = device_client.delete_device_by_deviceid(fake_device.device_id)
    ap1 = device_client.get_latest_app_version(fake_device.user_id)
    print ap1
    assert_device_is_not_exist(fake_device)
    print "1 end ------------------------------------------------------------------------------------------------------"

    # 2. create
    print fake_device
    rs = device_client.create_device(fake_device)
    assert rs.success is True
    assert_device_is_updated(fake_device)
    print "2 end ------------------------------------------------------------------------------------------------------"

    # 3. delete_device_by_userid
    rs = device_client.delete_device_by_userid(not_exist_uid)
    assert rs.success is True
    assert_device_is_not_exist(fake_device)
    print "3 end ------------------------------------------------------------------------------------------------------"

    # 4. create again
    rs = device_client.create_device(fake_device)
    assert rs.success is True
    assert_device_is_updated(fake_device)
    print "4 end ------------------------------------------------------------------------------------------------------"

    # 5. delete_device_by_device
    rs = device_client.delete_device_by_deviceid(device_id)
    assert rs.success is True
    assert_device_is_not_exist(fake_device)
    print "5 end ------------------------------------------------------------------------------------------------------"

    # 6. create again
    rs = device_client.create_device(fake_device)
    assert rs.success is True
    assert_device_is_updated(fake_device)
    print "6 end ------------------------------------------------------------------------------------------------------"

    # 7. update_device_by_userid
    fake_device.push_token = 'test_push_token2'
    rs = device_client.update_device_by_userid(fake_device)
    assert rs.success is True
    assert_device_is_updated(fake_device)
    print "7 end ------------------------------------------------------------------------------------------------------"

    # 8. update_device_by_deviceid
    fake_device.token_type = 2
    fake_device.app_version = 100000
    fake_device.enable_push = 0
    fake_device.updated_at = 12345
    fake_device.user_id = null_user_id
    rs = device_client.update_device_by_deviceid(fake_device)
    assert rs.success is True
    assert_device_is_updated(fake_device)
    print "8 end ------------------------------------------------------------------------------------------------------"

    # 9. delete_device_by_pushtoken
    rs = device_client.delete_device_by_pushtoken(fake_device.push_token)
    assert rs.success is True
    assert_device_is_not_exist(fake_device)
    print "9 end ------------------------------------------------------------------------------------------------------"

    # 10. get_latest_app_version
    ap1 = device_client.get_latest_app_version(fake_device.user_id)
    print ap1.app_version, fake_device.app_version
    if fake_device.user_id != null_user_id:
        assert ap1.app_version == fake_device.app_version


if __name__ == "__main__":
    pass
    test()
    # print device_client.get_latest_app_version(30598364)
    # print device_client.get_device_by_userid(31905455)
    # print device_client.get_device_by_deviceid("test_device_id")
    # print device_client.get_devices_by_userids([4065020, 32238582, 4065020])
