#coding=utf-8

from tornado.testing import AsyncTestCase
from tornado.testing import gen_test

import rpc_rel_cli

class ProcessorTestCase(AsyncTestCase):

    @gen_test
    def test_rel_get_black_list(self):
        relclient = rpc_rel_cli.RelClient()
        res1 = relclient.getBlackList(1)
        print res1
        async_rel_client = rpc_rel_cli.AsyncRelClient()
        res2 = yield async_rel_client.getBlackList(1)
        print res2
        self.assertEqual(res1, res2)

if __name__ == '__main__':
    import unittest
    unittest.main()
