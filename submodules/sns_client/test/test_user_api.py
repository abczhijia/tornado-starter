#coding=utf-8

import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../qbservice/gen-py"))
sys.path.append(".")
sys.path.append("..")
from thrift_sync_cli import get_auto_retry_user_client


user_client = get_auto_retry_user_client()


def test_update_user_mobile(user_id, mobile):
    user_client.update_user_mobile(int(user_id), mobile)


if __name__ == '__main__':
    pass
    # test_update_user_mobile(-1, '')

