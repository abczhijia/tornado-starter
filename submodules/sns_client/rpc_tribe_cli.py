# -*- coding: utf-8 -*-

from tornado.gen import coroutine, Return
from rpc_taf import Client, AsyncClient


DEFAULT_TRIBE_HOST_LIST = ["10.66.172.6:10301"]


class TribeClient(Client):

    def __init__(self, hosts=DEFAULT_TRIBE_HOST_LIST, version=0x03, timeout=3):
        super(TribeClient, self).__init__(hosts, version, timeout)

    # 获取群tribe_id的成员, 排除fromid
    def get_toids(self, tribe_id, fromid):
        rsp = self._process_user_request('get_toids', {'tribe_id': tribe_id, 'uid': fromid})  # [u'23643645', u'16818159']
        return map(int, rsp)

    # 获取用户uid加入的群的第page页
    def get_tribe_id_list(self, uid, page, count):
        rsp = self._process_user_request('get_tribe_id_list', {'uid': uid, 'page': page, 'count': count})
        return rsp  # [[u'14296', u'30664', u'16'], 3, False]

    # 获取用户uid加入的所有群
    def get_all_tribe_ids(self, uid):
        return map(int, self.get_tribe_id_list(uid, 1, -1)[0])


class AsyncTribeClient(AsyncClient):

    def __init__(self, server_list=DEFAULT_TRIBE_HOST_LIST, socket_timeout=2):
        super(AsyncTribeClient, self).__init__(server_list, socket_timeout)

    @coroutine
    def get_toids(self, tribe_id, fromid):
        rsp = yield self._process_user_request('get_toids', {'tribe_id': tribe_id, 'uid': fromid})
        raise Return(map(int, rsp))

    @coroutine
    def get_tribe_id_list(self, uid, page, count):
        rsp = yield self._process_user_request('get_tribe_id_list', {'uid': uid, 'page': page, 'count': count})
        raise Return(rsp)

    @coroutine
    def get_all_tribe_ids(self, uid):
        rsp = yield self.get_tribe_id_list(uid, 1, -1)
        tribe_ids = rsp[0]
        raise Return(map(int, tribe_ids))


if __name__ == '__main__':
    import time
    tribe_client = TribeClient()
    t1 = time.time()
    print tribe_client.get_toids(30664, 4065020)  # 30664 --> fortest
    t2 = time.time()
    print tribe_client.get_toids('30664', '4065020')  # 测兼容
    t2 = time.time()
    print tribe_client.get_all_tribe_ids(4065020)
    t3 = time.time()
    print tribe_client.get_all_tribe_ids('4065020')  # 测兼容
    print 'get_toids:', t2 - t1
    print 'get_all_tribe_ids', t3 - t2

    from tornado.ioloop import IOLoop
    from functools import partial
    async_tribe_client = AsyncTribeClient()
    print IOLoop.current().run_sync(partial(async_tribe_client.get_toids, 30664, 4065020))
    print IOLoop.current().run_sync(partial(async_tribe_client.get_all_tribe_ids, 4065020))

