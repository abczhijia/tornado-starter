# coding=utf-8

import redis
import logging


redis_host = "10.207.169.69"
redis_port = 16379
g_user_key = "bitmap_%s"
# 添加新的开关, 注意需要更新项目: user_bitmap, imserver, push_new
g_bits = ("bitInit",          # 标记该用户是否更改过任意一个开关, 没改过的话, 此用户的bits数据不用存入redis
          "pushImTotal",      # 已废弃: 早期的IM消息总开关, 现细分为几个开关
          "pushImSound",      # 声音
          "pushImVibrate",    # 震动
          "pushImDetail",     # 已废弃: 通知栏是否显示消息详细信息, 现全部打开
          "pushAntiDisturb",  # 夜间免打扰(初始值0)
          "pushArticle",      # 糗事精选推送(单条糗事)
          "pushNewFan",       # 新粉丝
          "pushTribe",        # 已废弃: 早期所有群的总消息通知开关, 现换成每个用户在每个群独立一个开关
          "pushTribeTmp",     # 群临时小纸条
          "pushPublic",       # 已废弃: 早期所有官号共用一个开关, 现每个官号独立开关
          "pushCircle",       # 糗友圈消息
          "pushQiushi",       # 糗事消息
          "pushTribeNotice",  # 群通知, 指的是帐号"群通知"发送的IM消息
          "pushSingle",       # 新小纸条
          "invisUserCenter",  # 1则屏蔽web个人中心数据(初始值0)
          "topic",            # 卧谈会(糗友圈)话题推送 (注: 字段和上面不一致, 是因为安卓用的此字段且已经发版很久了)
          "qiushiSmile",      # 糗事笑脸通知
          )
g_bits_set = set(g_bits)
g_bits_byte_len = (len(g_bits) + 7) / 8
g_bit_redis = redis.StrictRedis(host=redis_host, port=redis_port, socket_timeout=2)


g_init_bitmap = {}  # 初始状态
for _bit in g_bits:
    g_init_bitmap[_bit] = 1 if _bit not in ('bitInit', 'pushAntiDisturb', 'invisUserCenter') else 0

g_user_bitmap = {}  # init --> {'bitInit': 0, 'pushImTotal': 1, 'pushImSound': 2 ...}
for _i, _bit in enumerate(g_bits):
    g_user_bitmap[_bit] = _i


def set_bitmap(uid, bit, val, db=g_bit_redis):
    return db.setbit(g_user_key % uid, bit, val)


def set_bitmap_multi(uid, data):
    data.update({'bitInit': 1})
    p = g_bit_redis.pipeline()
    for k in data:
        if data[k] == 0 or data[k] == 1:
            set_bitmap(uid, g_user_bitmap[k], data[k], p)
    return p.execute()


# 获取一个用户的所有开关
def get_bitmap_multi(uid):
    try:
        data = g_bit_redis.get(g_user_key % uid)
        if not data:  # 用户未初始化过开关
            return g_init_bitmap

        # 新增1位开关(默认值为0, 故无需执行脚本初始化数据); 但是, 此时如果redis存储刚好需要新增1个字节, 会导致进入此if条件
        if len(data) < g_bits_byte_len:
            suffix = '\x00' * (g_bits_byte_len - len(data))
            data += suffix

        hex_str = data.encode('hex')           # str -> hex_str, '\xe1\xf8\x00' -> 'e1f800'
        bin_str = bin(int(hex_str, 16))[2:]    # hex_str -> int -> bin_str
        return dict((k, int(v)) for k, v in zip(g_bits, bin_str))
    except:  # 超时之类的异常, 降级返回初始值
        logging.exception('Error when get_bitmap_multi(%s)' % uid)
        return g_init_bitmap


# 获取一个用户的某个开关
def get_user_bitmap_by_type(uid, _type):
    if _type not in g_bits_set:
        assert 'push bitmap type is wrong'
    return get_bitmap_multi(uid)[_type]


def test():
    for uid in (4065020, 1):
        bits = get_bitmap_multi(uid)
        print len(bits), bits
        for bit in g_bits:
            print bit, get_user_bitmap_by_type(uid, bit)


if __name__ == "__main__":
    test()
