# -*- coding: utf-8 -*-

import sys
import fcntl
import socket
import struct
import redis
import json
import datetime


VERSION = 0x01

# LOG_SERVER
POST_ARCHIEVE = 0x01
WEB_HTTP_ERROR = 0x02
APP_HTTP_ERROR = 0x03
LONG_WEB_HTTP_REQUEST = 0x04
LONG_APP_HTTP_REQUEST = 0x05
USER_LIFE_TRACK = 0x06
INSERT_ERROR = 0x07
SEND_MAIL_STAT = 0x08
MSG_SERVER_ERROR = 0x09
ANALYSIS = 0x0A

LOG_SERVER_KEY = "log_server:analisys"

g_log_mq = redis.StrictRedis(host='10.251.101.154', port=16379)

# LOG_CENTER
WEB_TIMEOUT_ERROR = 0x01
APP_TIMEOUT_ERROR = 0x02

def get_local_ip(ifname='eth0'):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        inet = fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20: 24]
        return socket.inet_ntoa(inet)
    except:
        return '0.0.0.0'

LOCAL_IP = get_local_ip()

def send_udp_log(cmd, log):
    log = str(datetime.datetime.now()).split(".")[0]\
        + " [" + LOCAL_IP + "] " + log
    log_info = {"cmd": cmd, "log": log}
    g_log_mq.lpush(LOG_SERVER_KEY, json.dumps(log_info))

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "./exe cmd content"
        sys.exit(-1)

    send_udp_log(int(sys.argv[1]), sys.argv[2])
