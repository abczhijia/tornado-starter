# -*- coding: utf-8 -*-

import time
import logging

from celery import Celery
from celery.execute import send_task
import celeryconfig



app = Celery('email')
app.config_from_object(celeryconfig)


'''
# celery的worker代码, 最新代码请参照 项目async-tasks/mail.py

import requests

def _send(from_addr, to_addr, subject, html):
    request_url = MAILGUN_URL
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
    data = {
        'from': from_addr,
        'to': to_addr,
        'subject': subject,
        'html': html
    }
    requests.post(request_url, auth=MAILGUN_AUTH, headers=headers, data=data, timeout=10)

@app.task(bind=True, default_retry_delay=60, max_retries=5)
def send_mail(self, to_addr, subject, html,
              from_addr='糗事百科<no-reply@mail.qiushibaike.com>',
              tags={}):
    try:
        _send(from_addr, to_addr, subject, html)
        logging.info(u'request mailgun succ. to_addr: %s, subject: %s, tags: %s',
                     to_addr, subject, tags)
    except Exception as e:
        logging.warning(u'request mailgun fail. to_addr: %s, subject: %s, tags: %s',
                        to_addr, subject, tags)
        self.retry()
'''


def send_mail(to_addr, subject, content, from_addr='糗事百科<no-reply@mail.qiushibaike.com>', tags={}):
    t1 = time.time()
    send_task("mail.send_mail", args=[to_addr, subject, content, from_addr, tags], kwargs={})
    t2 = time.time()
    logging.info('mail_type[%s] email[%s] title[%s] spend[%.2fms]', tags.get('type', 'default'), to_addr, subject, t2 - t1)


if __name__ == '__main__':
    """
    项目中需要使用本模块发送邮件, 务必先pip install Celery
    """
    to = 'wulin@qiushibaike.com'
    send_mail(to, 'test single send', '内容')

    tos = 'wulin@qiushibaike.com;wulin@qiushibaike.com'
    send_mail(tos, 'test multi send', '内容')
