#!/usr/bin/env python
# -*- coding: utf-8

"""
数据上报接口
"""

import socket
import logging
from random import choice
from tornado.escape import json_encode

DEFAULT_RP_HOST_LIST = ["10.232.76.50:6670"]

class ReportClient(object):
    def __init__(self):
        self._server_list = DEFAULT_RP_HOST_LIST
        self._socket = None
        self.reconnect()

    def reconnect(self):
        if self._socket:
            self.close()
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        host, port = self.round_robin()
        s.settimeout(3)
        s.connect((host, port))
        self._socket = s

    def report(self, data):
        try:
            return self._fetch(data)
        except Exception, e:
            logging.warn("Reconnect.")
            self.reconnect()
            return self._fetch(data)

    def _fetch(self, data):
        self._socket.sendall(json_encode(data))

    def close(self):
        self._socket.close()
        self._socket = None

    def round_robin(self):
        addr = choice(self._server_list)
        pair = addr.split(':')
        return pair[0], int(pair[1])

if __name__ == '__main__':
    c = ReportClient()
    c.report({"t":"rel", "uid": 1, "ret_rel": "friend", "come_from": "{\"type\":1}"})
    c.close()
