#!/usr/bin/env python
# -*- coding: utf-8 -*-

BROKER_URL = 'redis://10.207.169.69:6379/0'
BROKER_CONNECTION_MAX_RETRIES = 0  # forever
BROKER_HEARTBEAT = 10

CELERY_IGNORE_RESULT = True
CELERY_ROUTES = {'mail.send_mail': {'queue': 'mails'}}
