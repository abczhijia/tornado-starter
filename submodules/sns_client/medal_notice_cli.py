#coding=utf-8

# 勋章通知 - IM接口

import time
import json
import logging

from new_im_cli import str_msgid_generate, send_p2p_msg


def send_medal_notice(toid, pic_url, new_text, old_text):
    fromid, fromnick, fromicon = 21481189, "坨坨", "20150202122548.jpg"
    im_msg = {
        "type": 28,
        "phone_ver": "4.0.0",
        "from": fromid,
        "fromnick": fromnick,
        "fromicon": fromicon,
        "to": toid,
        "msgid": str_msgid_generate(fromid, toid),
        "status": 1,
        "time": int(time.time() * 1000),
        "notify": True,
        "data": json.dumps({
            'pic_url': pic_url,
            'text': new_text
        }),
        "min_replace_keys": {
            "data": old_text,
            "type": 1,
        },
        "min_iv": 100300,  # iOS客户端上线版本(能够收到type:28的版本)
        "min_av": 100200,
    }
    send_p2p_msg(im_msg)
    logging.info(u'medal_noticle, msg: %s', im_msg)


if __name__ == '__main__':
    send_medal_notice(4065020, 'http://ww1.sinaimg.cn/large/65e4f1e6gw1f74wpwzzq6j2046046q2y.jpg',
                      '恭喜您获得勋章【一鸣惊人】1星成就！点击查看',
                      '恭喜您获得勋章【一鸣惊人】1星成就！快去升级最新版本查看吧~')
    send_medal_notice(23643645, 'http://ww1.sinaimg.cn/large/65e4f1e6gw1f74wpwzzq6j2046046q2y.jpg',
                      '恭喜您获得勋章【一鸣惊人】1星成就！点击查看',
                      '恭喜您获得勋章【一鸣惊人】1星成就！快去升级最新版本查看吧~')
