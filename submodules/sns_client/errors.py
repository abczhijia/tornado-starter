# -*- coding: utf-8 -*-


class AppError(Exception):

    def __init__(self, err, message):
        self.code = err
        self.message = message

    def __str__(self):
        message = self.message
        if isinstance(message, unicode):
            message = message.encode('utf-8')

        return '<%d %s>' % (self.code, message)

ErrTokenInvalid = AppError(107, u'用户登录失效,请重新登录')
ErrLoginAgain = AppError(108, u'请重新登录')
