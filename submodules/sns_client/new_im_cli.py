#coding=utf-8

import json
import time
import redis
import hashlib
import logging
import traceback


proxy_mq_list = ["proxymq%s" % i for i in xrange(3)]  # 优先级: proxymq0 > proxymq1 > proxymq2
proxy_redis_host = '10.251.74.150'  # 正式库
proxy_redis_port = 16393
proxy_redis_db = 0
im_mysql_host = '10.104.121.89'     # 正式库
im_mysql_db = 'tim'
im_mysql_user = 'im'
im_mysql_passwd = 'Im_2016_!025'


# lazy create connection
g_im_mysql = None
g_proxy_redis = redis.StrictRedis(host=proxy_redis_host, port=proxy_redis_port, db=proxy_redis_db,
                                  retry_on_timeout=True, socket_connect_timeout=3)


# NOTE MySQL的连接权限
def init_im_mysql():
    import torndb
    global g_im_mysql
    g_im_mysql = torndb.Connection(host=im_mysql_host, database=im_mysql_db, user=im_mysql_user, password=im_mysql_passwd,
                                   connect_timeout=3)


# 模拟客户端, 生成im_msg消息体内的msgid字符串
g_md5 = hashlib.md5()
def str_msgid_generate(fromid, toid):
    g_md5.update("%s_%s_%s" % (int(time.time() * 1000), fromid, toid))
    return g_md5.hexdigest()[:16]


def _put_into_proxymq(item, priority):
    assert 0 <= priority <= 2
    mq = proxy_mq_list[priority]
    g_proxy_redis.lpush(mq, json.dumps(item))


# 各个通知号触发的通知消息
def send_p2p_msg(im_msg):
    item = {
        'type': 'proxy_p2p',
        'fid': im_msg['from'],
        'tid': im_msg['to'],
        'im_msg': im_msg,
    }
    _put_into_proxymq(item, 0)


# 群(系统)消息
def send_tribe_msg(im_msg):
    item = {
        'type': 'proxy_tribe',
        'fid': im_msg['from'],
        'tid': im_msg['gid'],
        'im_msg': im_msg,
    }
    _put_into_proxymq(item, 0)


# 为im_msg(dict类型)在push_msg表中创建一行记录, 返回其push_id
def _create_mysql_push_msg(im_msg):
    global g_im_mysql
    if not g_im_mysql:
        init_im_mysql()
    msg_type, fromid, created_at = im_msg['type'], im_msg['from'], im_msg['time']
    data = json.dumps(im_msg)
    sql = "insert into push_msg(type, fromid, created_at, data) values(%s, %s, %s, %s)"
    try:
        logging.info(u'create_push_msg, type:%s, fromid:%s, created_at:%s, data:%s', msg_type, fromid, created_at, data)
        return g_im_mysql.execute(sql, msg_type, fromid, created_at, data)
    except:
        logging.warn(u'error when _create_mysql_push_msg(%s, %s, %s, %s), exception: %s' %
                     (msg_type, fromid, created_at, data, traceback.format_exc()))
        if g_im_mysql is not None:
            g_im_mysql.close()
        init_im_mysql()
        return g_im_mysql.execute(sql, msg_type, fromid, created_at, data)


# 使用push_id(_create_msg_push_msg函数返回的) 和 im_msg(dict类型), 推送给toids
def batch_send_msg(push_id, im_msg, toids, priority=1, page_count=50, sleep_time=0, log_interval=10):
    length = len(toids)
    count = (length + page_count - 1) / page_count
    for i in xrange(count):
        tids = toids[i * page_count: (i + 1) * page_count]
        item = {
            'type': 'proxy_batch_impush',
            'fid': int(im_msg['from']),
            'tids': tids,
            'push_id': push_id,
            'im_msg': im_msg,
        }
        _put_into_proxymq(item, priority)
        if sleep_time:
            time.sleep(sleep_time)
        if i % log_interval == 0:
            logging.info(u'page: %s, tids: %s', i + 1, tids)

