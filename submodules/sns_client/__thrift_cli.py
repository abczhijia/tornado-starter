# -*- coding: utf-8 -*-

import re
import time
import socket
import inspect
import numbers
import logging
from datetime import timedelta
from collections import deque
from thrift.Thrift import TException
from thrift.transport import TSocket
from thrift.TTornado import TTornadoStreamTransport
from thrift.transport.TTransport import TTransportException
from tornado.escape import xhtml_unescape
from tornado.iostream import StreamClosedError
from tornado.gen import coroutine, with_timeout, Return

from sns_conf import convert

TIME_ZONE = time.timezone

DEFAULT_TIMEOUT = 3

DEFAULT_SERVICE_COMMENT_HOST = '10.66.153.189'
DEFAULT_SERVICE_COMMENT_PORT = 8200
DEFAULT_SERVICE_ARTICLE_HOST = '10.66.133.211'
DEFAULT_SERVICE_ARTICLE_PORT = 9000
DEFAULT_SERVICE_DEVICE_HOST = '10.66.175.52'
DEFAULT_SERVICE_DEVICE_PORT = 8400
DEFAULT_SERVICE_USER_HOST = '10.66.201.79'
DEFAULT_SERVICE_USER_PORT = 8500
STATUS_MAP = {"waiting": "pending", "reported": "publish", "fake": "publish", "deleted": "publish"}


class ConnectionExhausting(Exception):
    pass


class TClient(object):

    def __init__(self, host, port, service, protocol, connect_timeout=1,
                 idle_timeout=600, max_active_conns=10, max_idle_conns=5):
        self._host = host
        self._port = port
        self._service = service
        self._protocol = protocol
        self._idle_connections = deque()
        self._idle_timeout = idle_timeout
        self._connect_timeout = connect_timeout
        self._max_idle_conns = max_idle_conns
        self._max_active_conns = max_active_conns
        self._active_conn_num = 0

        # 继承依然无法实现注入,因此,在创建连接池时动态注入函数,需要注意的时,此处默认所有服务不为rpc Client的私有成员.
        for name, value in inspect.getmembers(self._service.Iface, predicate=inspect.ismethod):
            setattr(self, name, self._serve(name))

    @property
    def remote_addr(self):
        return '%s:%s' % (self._host, self._port)

    @coroutine
    def _connect(self):
        """
        连接池上货
        :return:
        """
        transport = TTornadoStreamTransport(self._host, self._port)
        conn = yield transport.open(timeout=timedelta(seconds=self._connect_timeout))
        client = self._service.Client(conn, self._protocol)
        raise Return(client)

    def close(self):
        """
        销毁连接池
        :return:
        """
        while self._idle_connections:
            client = self._idle_connections.popleft()
            if not client._transport.stream.closed():
                client._transport.stream.close()

    @coroutine
    def _get(self):
        """
        连接池中取得连接,在有连接池的情况下,连接创建和关闭的次数不会很多
        :return: rpc Client
        """
        if self._active_conn_num > self._max_active_conns:
            raise ConnectionExhausting("%s current active clients exceed: %s"
                                       % (self.remote_addr, self._active_conn_num))
        client = None
        # 清理空闲连接
        while self._idle_connections:
            c = self._idle_connections[0]
            if c._idle_at + self._idle_timeout > time.time():
                break
            c = self._idle_connections.popleft()
            if not c._transport.stream.closed():
                c._transport.stream.close()
            logging.info('%s prune idle client, current active clients count: %s idle clients count: %s'
                         % (self.remote_addr, self._active_conn_num, len(self._idle_connections)))
        # 每次都是拿最近用过的连接，防止连接循环使用，释放不了
        if self._idle_connections:
            client = self._idle_connections.pop()
        if not client:
            client = yield self._connect()
            logging.info('%s create new thrift client, current active clients count: %s idle clients count: %s'
                         % (self.remote_addr, self._active_conn_num + 1, len(self._idle_connections)))
        self._active_conn_num += 1
        raise Return(client)

    def _put(self, client):
        """
        归还连接
        :param client: rpc Client
        :return:
        """
        self._active_conn_num -= 1
        if client._transport.stream.closed():
            return
        while self._idle_connections:
            conn = self._idle_connections[0]
            if not conn or conn._idle_at + self._idle_timeout > time.time():
                break
            conn = self._idle_connections.popleft()
            if not conn._transport.stream.closed():
                conn._transport.stream.close()
            logging.info('%s put back prune idle client, current active clients count: %s idle clients count: %s'
                         % (self.remote_addr, self._active_conn_num, len(self._idle_connections)))
        current_idle_conn_num = len(self._idle_connections)
        if current_idle_conn_num > self._max_idle_conns:
            client._transport.stream.close()
            logging.info('%s current idle clients exceed: %s' % (self.remote_addr, current_idle_conn_num))
            return
        client._idle_at = time.time()
        self._idle_connections.append(client)

    def _serve(self, func):
        """
        服务注入
        :param func: 服务名
        :return:
        """

        @coroutine
        def _(*args, **keys):
            # 服务器返回异常类型
            timeout = keys.pop("timeout", DEFAULT_TIMEOUT)
            assert isinstance(timeout, numbers.Real), "timeout should be a real number!"
            retry = 1
            while retry >= 0:
                client = yield self._get()
                try:
                    future = getattr(client, func)(*args, **keys)
                    if timeout > 0:
                        future = with_timeout(timedelta(seconds=timeout), future,
                                              quiet_exceptions=(TTransportException, StreamClosedError))
                    result = yield future
                    break
                except TTransportException as e:
                    client._transport.stream.close()
                    # TTransportException异常重试
                    if retry == 0:
                        raise
                    retry -= 1
                # thrift定义异常，直接抛出
                except TException as e:
                    raise e
                except Exception as e:
                    client._transport.stream.close()
                    raise e
                finally:
                    self._put(client)

            raise Return(result)

        return _


# === 同步封装 === #
class Socket(TSocket.TSocket):
    """ 重写socket，支持重连 """

    def __init__(self, host, port, timeout=1):
        TSocket.TSocket.__init__(self, host, port)
        self.setTimeout(timeout * 1000)
        self.reconnect()

    def reconnect(self):
        if self.isOpen(): self.close()
        self.open()

    def read(self, size):
        try:
            return TSocket.TSocket.read(self, size)
        except (socket.error, IOError) as e:
            logging.warn(e)
            self.close()
            raise TTransportException(type=TTransportException.END_OF_FILE, message=str(e))
        except Exception as e:
            logging.warn(e)
            self.close()
            raise TTransportException(type=TTransportException.NOT_OPEN, message=str(e))

    def write(self, buff):
        try:
            return TSocket.TSocket.write(self, buff)
        except Exception as e:
            logging.error(e)
            self.reconnect()
            # 连接重连后，数据没有写出去，导致read阻塞
            TSocket.TSocket.write(self, buff)


def unfilter(s):
    s = re.sub(r"(?!5{3,}|2{3,})\d{6,}", "**", s) if s else ''
    return xhtml_unescape(s)


def get_pic_name(filename):
    """
    获得图片的实际名称
    :param filename:
    :return:
    """
    filename = filename.split('.')[0] + ".jpg"
    return filename


def get_pic_size(picture_content_type):
    """
    获得资源(图片或视频)的长宽信息
    """
    if picture_content_type:
        try:
            lines = picture_content_type.split("\n")
            img_size = {}
            for line in lines:
                if not line:
                    continue
                parts = line.split(":")
                k = parts[0].strip()
                v = parts[1].strip()
                val = map(int, v[1:-1].split(','))
                img_size[k] = val
        except:
            return None
        if isinstance(img_size, dict):
            return img_size
    return None


def make_user_info(user, with_email=False):
    return make_user_detail(user, with_email, brief=True)


def make_user_detail(info, with_email=False, brief=False):
    '''提供一个只返回简洁信息的接口，只返回部分用户信息'''
    user_detail = dict(
            id=info.user_id,
            uid=info.user_id,
            login=info.login,
            icon=info.avatar,
            role=info.role,
            state=info.state,
            created_at=info.created_at,
            updated_at=info.updated_at,
            last_device=info.last_device,
            last_visited_at=info.last_visited_at,
            avatar_updated_at=info.avatar_updated_at,
            gender=info.gender,
            age=0,
            astrology=''
    )
    if with_email:
        user_detail['email'] = info.email
    if info.birthday:
        user_detail['age'] = convert.convert_birthday_to_age(info.birthday)
        user_detail['astrology'] = convert.convert_birthday_to_astrology(info.birthday)

    if not brief:
        user_detail.update(dict(
            location=info.location,
            hobby=info.hobby,
            signature=info.signature,
            introduce=info.introduce,
            emotion=info.emotion,
            bg=str(info.bg_id),
            qb_age=int(time.time() - info.created_at) / 86400 + 1,
            birthday=info.birthday and int(time.mktime(time.strptime(info.birthday.split(" ")[0], '%Y-%m-%d'))),

            big_cover=convert.convert_big_cover(info.big_cover),
            hometown=convert.convert_hometown(info.hometown),
            job=convert.convert_job(info.job),
            haunt=convert.convert_haunt(info.haunt),
            mobile_brand=convert.convert_mobile_brand(info.mobile_brand)
        ))

    return user_detail


def make_article_info(article):
    art = {
        'id': article.id,
        'votes': {'up': article.votes.up, 'down': article.votes.down},
        'user_id': article.user_id,
        'tag': unfilter(article.tag_line),
        'content': unfilter(article.content),
        'created_at': int(article.created_at) + TIME_ZONE,
        'allow_comment': article.comment_status in ['open', ''],
        'state': article.status,
        'image': article.picture_file_name if article.picture_file_name not in ["None", "NULL"] else None,
        'anonymous': 1 if article.user_id == 0 else article.anonymous,
        'comments_count': article.comments_count,
        'published_at': int(article.published_at),
        'share_count': article.share_count
    }
    if art['image']:
        if art['image'].startswith("gif") and art['image'][-4:] == '.mp4':
            art['image'] = art['image']
            art['format'] = 'gif'
        elif art['image'][-4:] == '.mp4':
            art['image'] = art['image']
            art['pic_size'] = [480, 480]
            art['format'] = 'video'
        else:
            art['image'] = get_pic_name(art['image'])
            art['format'] = 'image'
    else:
        art['format'] = 'word'
    if hasattr(article, 'image_size') and article.image_size and \
                    article.image_size not in ["None", "NULL"]:
        if isinstance(article.image_size, (str, unicode)):
            art["image_size"] = eval(article.image_size)
        else:
            art["image_size"] = article.image_size
    else:
        picture_content_type = article.picture_content_type \
            if article.picture_content_type not in ["None", "NULL"] else None
        try:
            art["image_size"] = get_pic_size(picture_content_type)
        except:
            art["image_size"] = None
    if hasattr(article, 'loop') and article.loop:
        art['loop'] = article.loop
    if hasattr(article, 'topic') and article.topic:
        art['topic'] = {
            "id": article.topic.id,
            "content": article.topic.content,
            "avatar": article.topic.avatar,
            "background": article.topic.background,
            "status": article.topic.status
        }
    return art


def make_comment_info(comment):
    return dict(
            id=comment.id,
            floor=comment.floor,
            content=comment.content,
            like_count=comment.like_count,
            created_at=int(time.mktime(time.strptime((comment.created_at), '%Y-%m-%d %H:%M:%S'))),
            parent_id=comment.parent_id,
            at_infos=comment.refers
    )
