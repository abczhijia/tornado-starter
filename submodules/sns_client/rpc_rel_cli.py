# -*- coding: utf-8 -*-

import time
import logging
from tornado import gen
from rpc_taf import Client, AsyncClient, retry, check_if_int


DEFAULT_REL_HOST_LIST = ["10.66.172.60:21001"]


class RelClient(Client):

    def __init__(self, hosts=DEFAULT_REL_HOST_LIST, version=0x03, timeout=3):
        super(RelClient, self).__init__(hosts, version, timeout)

    def getListCommon(self, cmd, uid):
        '''getList系列函数的公共部分'''
        rsp = self._process_user_request(cmd, {'uid': uid})
        return map(int, rsp) if rsp else []

    def getFollowList(self, uid):
        '''取所有uid关注的，包括单向和双向的'''
        rsp = self._process_user_request('doGetMyFollowAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    def getSingleFollowList(self, uid):
        '''取所有uid关注的，不包括双向的'''
        rsp = self._process_user_request('getFollowAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    def getFollowedList(self, uid):
        '''取所有关注uid的，包括单向和双向'''
        rsp = self._process_user_request('doGetMyFollowedAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    def getSingleFollowedList(self, uid):
        '''取所有关注uid的，不包括双向的'''
        rsp = self._process_user_request('getFollowedAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    def getFriendList(self, uid):
        '''取uid的所有好友（仅双向关系）'''
        rsp = self._process_user_request('getFriendAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    def getBlackList(self, uid):
        '''取uid的所有拉黑'''
        rsp = self._process_user_request('getBlackAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    def getChatedList(self, uid):
        '''取聊天列表(所有)'''
        rsp = self._process_user_request('getChatedAll', {'uid': uid})
        return map(int, rsp) if rsp else []

    # ---------------------------------------------------------------
    # ###############         2 start        ########################
    # ---------------------------------------------------------------
    # 功能:
    #       取得与用户uid有某种关系的第page页用户ID, 每页count个用户
    # 输入:
    #       uid: int
    #       page: int   第几页
    #       count: int  每页多少个
    # 输出:
    #     (data:[], total:int, has_more:bool)
    # ---------------------------------------------------------------
    @retry
    def getPageCommon(self, function, uid, page, count):
        err, (uid, page, count) = check_if_int(uid, page, count)
        if err:
            logging.error('error: input in getPageCommon is not int')
            return {'err': -1, 'err_msg': 'error_input'}
        if count > 50:
            count = 50
        kwargs = {'uid': uid, 'page': page, 'count': count}
        rsp = self.do_proc(function, kwargs)
        if not rsp:
            return ([], 0, False)
        return (rsp[1], rsp[0], page * count < rsp[0])

    def getFriend(self, uid, page, count):
        '''取uid的分页好友数据'''
        return self.getPageCommon("getFriend", uid, page, count)

    def getFollowed(self, uid, page, count):
        '''取uid的分页粉丝数据'''
        return self.getPageCommon("getFollowed", uid, page, count)

    def getFollow(self, uid, page, count):
        '''取uid的分页关注数据'''
        return self.getPageCommon("getFollow", uid, page, count)

    def getBlack(self, uid, page, count):
        '''取uid的分页黑名单数据'''
        return self.getPageCommon("getBlack", uid, page, count)

    # ---------------------------------------------------------------
    # ###############         3 start        ########################
    # ---------------------------------------------------------------
    # 功能:
    #       获取两个用户的关系
    # 输入:
    #       uid, toid   --> int
    # 输出:
    #       'relationship_string'   参考 节0.3
    # ----------------------------------------------------------------
    @retry
    def getRelation(self, uid, toid):
        '''获取两个用户的关系'''
        err, (uid, toid) = check_if_int(uid, toid)
        if err:
            logging.error('error: input in getRelation is not int')
            return 'error_input'
        kwargs = {"uid": uid, "toid": toid}
        rsp = self.do_proc("doGetRelation", kwargs)
        return rsp or 'no_rel'

    # ---------------------------------------------------------------
    # ###############         4 start        ########################
    # ---------------------------------------------------------------
    # 功能:
    #       获取用户uid与若干个用户toids的关系
    # 输入:
    #       uid     --> int
    #       toids   --> list, 元素类型为int
    # 输出:
    #      {
    #         uid1: 'relationship_string',  参考 节0.3
    #         uid2: 'relationship_string',
    #         ...
    #      }
    #      注: 键uid为int类型
    # ---------------------------------------------------------------
    @retry
    def getMultiRelation(self, uid, toids, relations=[]):
        '''获取用户uid与若干个用户toids的关系'''
        if not isinstance(toids, (list, tuple)):
            return {}
        err1, (uid,) = check_if_int(uid)
        err2, toids = check_if_int(*toids)
        if err1 or err2:
            logging.error('error: input in getMultiRelation is not int')
            return {}
        if isinstance(toids, tuple):
            toids = list(toids)
        kwargs = {'uid': uid, 'toids': toids, 'relations': relations}
        rsp = self.do_proc("doGetMultiRelation", kwargs)
        return dict([(int(key), value) for key, value in rsp.iteritems()]) if rsp else {}

    # ---------------------------------------------------------------
    # ###############         5 start        ########################
    # ---------------------------------------------------------------
    # 功能:
    #       用户uid针对用户toid作取消关注/加黑/移黑等操作
    # 输入:
    #       uid     --> int
    #       toid    --> int
    # 输出:
    #       'new_relationship_string'  参考 节0.3
    # ---------------------------------------------------------------
    @retry
    def doCommon(self, function, uid, toid):
        '''doXxx系列函数的公共部分'''
        err, (uid, toid) = check_if_int(uid, toid)
        if err:
            logging.error('error: input in doCommon is not int')
            return 'error_input'
        kwargs = {"uid": uid, "toid": toid}
        rsp = self.do_proc(function, kwargs)
        return rsp or ''

    def doUnFollow(self, uid, toid):
        '''取消关注'''
        return self.doCommon("doUnFollow", uid, toid)

    def doChated(self, uid, toid):
        '''聊天'''
        return self.doCommon("doChated", uid, toid)

    def doDeleteFan(self, uid, toid):
        '''删除粉丝'''
        return self.doCommon("doDeleteFan", uid, toid)

    def doAddBlack(self, uid, toid):
        '''加黑'''
        return self.doCommon("doAddBlack", uid, toid)

    def doUnBlack(self, uid, toid):
        '''移黑'''
        return self.doCommon("doUnBlack", uid, toid)

    def isBlacked(self, uid, toids):
        '''判断两人的是否黑名单关系'''
        rsp = self.do_proc('isBlacked', {'uid': uid, 'toids': toids})
        return dict([(int(key), value) for key, value in rsp.iteritems()]) if rsp else {}

    def isBlack(self, uid, toid):
        '''判断两人的是否黑名单关系'''
        rsp = self.doCommon('isBlack', uid, toid)
        return rsp or False

    # ---------------------------------------------------------------
    # ###############         6 start        ########################
    # ---------------------------------------------------------------
    # 功能:
    #       用户uid针对用户toid作关注操作
    # 输入:
    #       uid         --> int
    #       toid        --> int
    #       shake_time  --> int
    #       shake_count --> int
    #       come_from   --> string:   'qiushi', 'nearby', 'cafe'
    # 输出:
    #       'new_relationship_string'  参考 节0.3
    # ---------------------------------------------------------------
    @retry
    def doFollow(self, uid, toid, shake_time, shake_count, come_from):  # more params
        '''关注'''
        err, (uid, toid, shake_time, shake_count) = check_if_int(uid, toid, shake_time, shake_count)
        if err:
            logging.error('error: input in doFollow is not int')
            return 'error_input'
        kwargs = {
            "uid": uid,
            "toid": toid,
            "shake_time": shake_time,
            "shake_count": shake_count,
            "come_from": come_from
        }
        rsp = self.do_proc("doFollow", kwargs)
        return rsp or ''

    def isFollow(self, fromid, toid):
        """ fromid是否关注toid """
        rel_str = self.getRelation(fromid, toid)
        return True if rel_str in ('friend', 'follow_replied', 'follow_unreplied') else False

    # TODO 后面改成服务端使用pipeline获取这两个zset长度后返回
    def getFollowedCount(self, uid):
        """ 取uid的粉丝(包括朋友)总数 """
        _a, follow_count, _b = self.getFollowed(uid, 1, 1)
        _a, friend_count, _b = self.getFriend(uid, 1, 1)
        return follow_count + friend_count


class AsyncRelClient(AsyncClient):

    def __init__(self, server_list=DEFAULT_REL_HOST_LIST, socket_timeout=2):
        super(AsyncRelClient, self).__init__(server_list, socket_timeout)

    @gen.coroutine
    def doFollow(self, uid, toid, shake_time, shake_count, come_from):
        '''关注'''
        err, (uid, toid, shake_time, shake_count) = check_if_int(uid, toid, shake_time, shake_count)
        if err:
            logging.error('error: input in doFollow is not int')
            raise gen.Return('error_input')
        kwargs = {
            'uid': uid,
            'toid': toid,
            'shake_time': shake_time,
            'shake_count': shake_count,
            'come_from': come_from
        }
        resp = yield self.do_proc('doFollow', kwargs)
        raise gen.Return(resp or '')

    @gen.coroutine
    def doCommon(self, function, uid, toid):
        '''doXxx系列函数的公共部分'''
        rsp = yield self.do_proc(function, {'uid': uid, 'toid': toid})
        raise gen.Return(rsp or '')

    @gen.coroutine
    def doUnFollow(self, uid, toid):
        '''取消关注'''
        resp = yield self.doCommon('doUnFollow', uid, toid)
        raise gen.Return(resp)

    @gen.coroutine
    def doChated(self, uid, toid):
        '''聊天'''
        resp = yield self.doCommon('doChated', uid, toid)
        raise gen.Return(resp)

    @gen.coroutine
    def doDeleteFan(self, uid, toid):
        '''删除粉丝'''
        resp = yield self.doCommon('doDeleteFan', uid, toid)
        raise gen.Return(resp)

    @gen.coroutine
    def doAddBlack(self, uid, toid):
        '''加黑'''
        resp = yield self.doCommon('doAddBlack', uid, toid)
        raise gen.Return(resp)

    @gen.coroutine
    def doUnBlack(self, uid, toid):
        '''移黑'''
        resp = yield self.doCommon('doUnBlack', uid, toid)
        raise gen.Return(resp)

    @gen.coroutine
    def getListCommon(self, cmd, uid):
        rsp = yield self._process_user_request(cmd, {'uid': uid})
        raise gen.Return(map(int, rsp) if rsp else [])

    @gen.coroutine
    def getFriendList(self, user_id):
        '''取uid的所有好友（仅双向关系）'''
        resp = yield self.getListCommon('getFriendAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getFollowList(self, user_id):
        '''取所有uid关注的，包括单向和双向的'''
        resp = yield self.getListCommon('doGetMyFollowAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getFollowedList(self, user_id):
        resp = yield self.getListCommon('doGetMyFollowedAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getSingleFollowList(self, user_id):
        '''取所有uid关注的，不包括双向的'''
        resp = yield self.getListCommon('getFollowAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getSingleFollowedList(self, user_id):
        '''取所有关注uid的，不包括双向的'''
        resp = yield self.getListCommon('getFollowedAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getBlackList(self, user_id):
        resp = yield self.getListCommon('getBlackAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getChatedList(self, user_id):
        '''取聊天列表(所有)'''
        resp = yield self.getListCommon('getChatedAll', user_id)
        raise gen.Return(resp)

    @gen.coroutine
    def getRelation(self, uid, toid):
        '''获取两个用户的关系'''
        rsp = yield self.do_proc('doGetRelation', {'uid': uid, 'toid': toid})
        raise gen.Return(rsp or 'no_rel')

    @gen.coroutine
    def getMultiRelation(self, uid, toids, relations=[]):
        '''获取用户uid与若干个用户toids的关系'''
        rsp = yield self.do_proc('doGetMultiRelation', {'uid': uid, 'toids': toids, 'relations': relations})
        raise gen.Return(dict([(int(key), value) for key, value in rsp.iteritems()]) if rsp else {})

    @gen.coroutine
    def isBlacked(self, uid, toids):
        '''获取用户uid与若干个用户toids的关系'''
        rsp = yield self.do_proc('isBlacked', {'uid': uid, 'toids': toids})
        raise gen.Return(dict([(int(key), value) for key, value in rsp.iteritems()]) if rsp else {})

    @gen.coroutine
    def isBlack(self, uid, toid):
        '''判断两人的是否黑名单关系'''
        rsp = yield self.do_proc('isBlack', {'uid': uid, 'toid': toid})
        raise gen.Return(rsp or False)

    @gen.coroutine
    def getPageCommon(self, function, uid, page, count):
        err, (uid, page, count) = check_if_int(uid, page, count)
        if err:
            logging.error('error: input in getPageCommon is not int')
            raise gen.Return({'err': -1, 'err_msg': 'error_input'})
        if count > 100:
            count = 100
        kwargs = {'uid': uid, 'page': page, 'count': count}
        rsp = yield self.do_proc(function, kwargs)
        if not rsp:
            raise gen.Return(([], 0, False))
        raise gen.Return((rsp[1], rsp[0], page * count < rsp[0]))

    @gen.coroutine
    def getFriend(self, uid, page, count):
        '''取uid的分页好友数据'''
        rs = yield self.getPageCommon("getFriend", uid, page, count)
        raise gen.Return(rs)

    @gen.coroutine
    def getFollowed(self, uid, page, count):
        '''取uid的分页粉丝数据'''
        rs = yield self.getPageCommon("getFollowed", uid, page, count)
        raise gen.Return(rs)

    @gen.coroutine
    def isFollow(self, fromid, toid):
        """ fromid是否关注toid """
        rel_str = yield self.getRelation(fromid, toid)
        raise gen.Return(True if rel_str in ('friend', 'follow_replied', 'follow_unreplied') else False)

    # TODO 后面改成服务端使用pipeline获取这两个zset长度后返回
    @gen.coroutine
    def getFollowedCount(self, uid):
        """ 取uid的粉丝(包括朋友)总数 """
        _a, follow_count, _b = yield self.getFollowed(uid, 1, 1)
        _a, friend_count, _b = yield self.getFriend(uid, 1, 1)
        raise gen.Return(follow_count + friend_count)


if __name__ == '__main__':
    relclient = RelClient()
    # print relclient.doFollow(23643645, 4065020, 3, 23, 'nearby')
    # print relclient.doUnFollow(4065020, 'a')
    # print relclient.getFollowList(4065020)
    # print relclient.getChatedList(4065020)
    # print relclient.getBlackList(4065020)
    # print relclient.getSingleFollowList(4065020)
    t1 = time.time()
    print relclient.getFollowedCount(32133759)
    t2 = time.time()
    print t2 - t1
    # print relclient.getSingleFollowedList(4065020)
    # print relclient.getFriend(4065020, 1, 3)
    # print relclient.getFollow(4065020, 1, 3)
    # print relclient.getBlack(4065020, 1, 3)
    # print relclient.getRelation(4065020, 1)
    # print relclient.doUnFollow(4065020, 23643645)
    # print relclient.doUnFollow(23643645, 4065020)
    # print relclient.doFollow(23643645, 4065020, 3, 23, 'nearby')
    # print relclient.doAddBlack(23643645, 4065020)
    # print relclient.doUnBlack(23643645, 4065020)
    # print relclient.doFollow(4065020, 23643645, 3, 23, 'nearby')
    print relclient.getMultiRelation(4065020, [1, '23643645'])
    print relclient.getMultiRelation(4065020, (1, 23643645))
    print relclient.getMultiRelation(4065020, ('1', 23643645))
    print relclient.getMultiRelation(4065020, ['a', 23643645, 3333])
    print relclient.getRelation(4065020, 23643645)
    print relclient.isFollow(4065020, 23643645)
    print relclient.isBlack(4065020, 1)
