# coding:utf8

# from tornado_mysql import pools
from config.mysql_config import config
from db_pool import db

async_db = db.DB(host=config['host'], port=config['port'], database=config['db'], user=config['user'],
                 password=config['passwd'],
                 init_command="set names utf8")
